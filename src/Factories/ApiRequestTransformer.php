<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/16/18
 * Time: 5:09 PM
 */

namespace Skipper\Telegram\Factories;

use Skipper\Telegram\Contracts\ContactContract;
use Skipper\Telegram\Contracts\LocationContract;
use Skipper\Telegram\Contracts\TextContract;
use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Contracts\VenueContract;
use Skipper\Telegram\Sendable\AbstractMessage;
use Skipper\Telegram\Sendable\AudioMessage;
use Skipper\Telegram\Sendable\CallbackQueryAnswer;
use Skipper\Telegram\Sendable\ChatActionMessage;
use Skipper\Telegram\Sendable\ContactMessage;
use Skipper\Telegram\Sendable\DocumentMessage;
use Skipper\Telegram\Sendable\EditMessageCaption;
use Skipper\Telegram\Sendable\EditMessageReplyMarkUp;
use Skipper\Telegram\Sendable\EditMessageText;
use Skipper\Telegram\Sendable\ForwardMessage;
use Skipper\Telegram\Sendable\Inline\BaseInlineResult;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultArticle;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultAudio;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultContact;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultDocument;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultGif;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultLocation;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultMpeg4Gif;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultPhoto;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultVenue;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultVideo;
use Skipper\Telegram\Sendable\Inline\InlineQueryResultVoice;
use Skipper\Telegram\Sendable\InlineResponse;
use Skipper\Telegram\Sendable\LocationMessage;
use Skipper\Telegram\Sendable\ManageUserInChatAction;
use Skipper\Telegram\Sendable\Message;
use Skipper\Telegram\Sendable\PhotoMessage;
use Skipper\Telegram\Sendable\SimpleEditMessage;
use Skipper\Telegram\Sendable\SimpleMessage;
use Skipper\Telegram\Sendable\StickerMessage;
use Skipper\Telegram\Sendable\VenueMessage;
use Skipper\Telegram\Sendable\VideoMessage;
use Skipper\Telegram\Sendable\VoiceMessage;
use Skipper\Telegram\Traits\HasAttachment;
use Skipper\Telegram\ValueObjects\ForceReply;
use Skipper\Telegram\ValueObjects\Inline\Content\ContactContent;
use Skipper\Telegram\ValueObjects\Inline\Content\LocationContent;
use Skipper\Telegram\ValueObjects\Inline\Content\TextMessageContent;
use Skipper\Telegram\ValueObjects\Inline\Content\VenueContent;
use Skipper\Telegram\ValueObjects\InlineKeyboardButton;
use Skipper\Telegram\ValueObjects\InlineKeyboardMarkup;
use Skipper\Telegram\ValueObjects\KeyboardButton;
use Skipper\Telegram\ValueObjects\ReplyKeyboardHide;
use Skipper\Telegram\ValueObjects\ReplyKeyboardMarkup;

class ApiRequestTransformer
{
    /**
     * @param ForwardMessage $message
     * @return array
     */
    public function createFromForward(ForwardMessage $message): array
    {
        return array_merge($this->createFromAbstractMessage($message), [
            'from_chat_id' => $message->getFromChatId(),
            'message_id' => $message->getMessageId(),
            'disable_notification' => $message->getDisableNotification(),
        ]);
    }

    /**
     * @param AbstractMessage $message
     * @return array
     */
    protected function createFromAbstractMessage(AbstractMessage $message)
    {
        return [
            'chat_id' => $message->getChatId(),
        ];
    }

    /**
     * @param PhotoMessage $photo
     * @return array
     * @throws \ReflectionException
     */
    public function createFromPhoto(PhotoMessage $photo): array
    {
        return array_merge(
            $this->createFromSimpleMessage($photo),
            $this->createFromFile($photo, 'photo')
        );
    }

    /**
     * @param SimpleMessage $message
     * @return array
     */
    protected function createFromSimpleMessage(SimpleMessage $message)
    {
        return array_merge($this->createFromAbstractMessage($message), [
            'disable_notification' => $message->getDisableNotification(),
            'reply_to_message_id' => $message->getReplyToMessageId(),
            'reply_markup' => $this->createReplyMarkUp($message->getReplyMarkUp()),
        ]);
    }

    /**
     * @param ForceReply|InlineKeyboardMarkup|ReplyKeyboardHide|ReplyKeyboardMarkup|null $markUp
     * @return array|null
     */
    protected function createReplyMarkUp($markUp)
    {
        switch (true) {
            case ($markUp instanceof ForceReply):
                return $this->createForceReply($markUp);
            case ($markUp instanceof InlineKeyboardMarkup):
                return $this->createInlineKeyboard($markUp);
            case ($markUp instanceof ReplyKeyboardHide):
                return $this->createReplyKeyboardHide($markUp);
            case ($markUp instanceof ReplyKeyboardMarkup):
                return $this->createReplyKeyboardMarkup($markUp);
            default:
                return null;
        }
    }

    /**
     * @param ForceReply $forceReply
     * @return array
     */
    public function createForceReply(ForceReply $forceReply): array
    {
        return [
            'force_reply' => $forceReply->isForceReply(),
            'selective' => $forceReply->getSelective(),
        ];
    }

    /**
     * @param InlineKeyboardMarkup $inlineKeyboardMarkup
     * @return array
     */
    public function createInlineKeyboard(InlineKeyboardMarkup $inlineKeyboardMarkup): array
    {
        $buttonsData = [];
        foreach ($inlineKeyboardMarkup->getInlineKeyboard() as $row => $buttons) {
            /** @var InlineKeyboardButton $button */
            foreach ($buttons as $button) {
                $buttonsData[$row][] = [
                    'text' => $button->getText(),
                    'url' => $button->getUrl(),
                    'callback_data' => $button->getCallbackData(),
                    'switch_inline_query' => $button->getSwitchInlineQuery(),
                    'switch_inline_query_current_chat' => $button->getSwitchInlineQueryCurrentChat(),
                ];
            }
        }

        return [
            'inline_keyboard' => $buttonsData,
        ];
    }

    /**
     * @param ReplyKeyboardHide $keyboardHide
     * @return array
     */
    public function createReplyKeyboardHide(ReplyKeyboardHide $keyboardHide): array
    {
        return [
            'hide_keyboard' => $keyboardHide->isHideKeyboard(),
            'selective' => $keyboardHide->getSelective(),
        ];
    }

    /**
     * @param ReplyKeyboardMarkup $keyboardMarkup
     * @return array
     */
    public function createReplyKeyboardMarkup(ReplyKeyboardMarkup $keyboardMarkup): array
    {
        $buttonsData = [];
        foreach ($keyboardMarkup->getKeyboard() as $row => $buttons) {
            /** @var KeyboardButton $button */
            foreach ($buttons as $button) {
                $buttonsData[$row][] = [
                    'text' => $button->getText(),
                    'request_contact' => $button->getRequestContact(),
                    'request_location' => $button->getRequestLocation(),
                ];
            }
        }

        return [
            'keyboard' => $buttonsData,
            'resize_keyboard' => $keyboardMarkup->getResizeKeyboard(),
            'one_time_keyboard' => $keyboardMarkup->getOneTimeKeyboard(),
            'selective' => $keyboardMarkup->getSelective(),
        ];
    }

    /**
     * @param SimpleMessage|\Skipper\Telegram\Traits\HasAttachment $message
     * @param string $fieldName
     * @return array
     * @throws \ReflectionException
     */
    protected function createFromFile(SimpleMessage $message, string $fieldName)
    {
        $reflection = new \ReflectionClass($message);
        if (false === in_array(HasAttachment::class, (array)$reflection->getTraitNames())) {
            return [];
        }
        return [
            $fieldName => $message->getFile(),
            'caption' => $message->getCaption(),
        ];
    }

    /**
     * @param StickerMessage $sticker
     * @return array
     * @throws \ReflectionException
     */
    public function createFromSticker(StickerMessage $sticker): array
    {
        return array_merge($this->createFromSimpleMessage($sticker), $this->createFromFile($sticker, 'sticker'));
    }

    /**
     * @param VoiceMessage $voice
     * @return array
     * @throws \ReflectionException
     */
    public function createFromVoice(VoiceMessage $voice): array
    {
        return array_merge(
            $this->createFromSimpleMessage($voice),
            $this->createFromFile($voice, 'voice'),
            [
                'duration' => $voice->getDuration(),
            ]
        );
    }

    /**
     * @param VideoMessage $video
     * @return array
     * @throws \ReflectionException
     */
    public function createFromVideo(VideoMessage $video): array
    {
        return array_merge(
            $this->createFromSimpleMessage($video),
            $this->createFromFile($video, 'video'),
            [
                'duration' => $video->getDuration(),
                'width' => $video->getWidth(),
                'height' => $video->getHeight(),
            ]
        );
    }

    /**
     * @param ContactMessage $contact
     * @return array
     */
    public function createFromContactMessage(ContactMessage $contact): array
    {
        return array_merge($this->createFromSimpleMessage($contact), $this->createFromContact($contact));
    }

    /**
     * @param ContactContract $content
     * @return array
     */
    protected function createFromContact(ContactContract $content): array
    {
        return [
            'phone_number' => $content->getPhoneNumber(),
            'first_name' => $content->getFirstName(),
            'last_name' => $content->getLastName(),
        ];
    }

    /**
     * @param ChatActionMessage $action
     * @return array
     */
    public function createFromChatAction(ChatActionMessage $action): array
    {
        return array_merge($this->createFromAbstractMessage($action), [
            'action' => $action->getAction(),
        ]);
    }

    /**
     * @param VenueMessage $venue
     * @return array
     */
    public function createFromVenueMessage(VenueMessage $venue): array
    {
        return array_merge($this->createFromLocationMessage($venue), $this->createFromVenue($venue));
    }

    /**
     * @param LocationMessage $location
     * @return array
     */
    public function createFromLocationMessage(LocationMessage $location): array
    {
        return array_merge($this->createFromSimpleMessage($location), $this->createFromLocation($location));
    }

    /**
     * @param LocationContract $content
     * @return array
     */
    protected function createFromLocation(LocationContract $content): array
    {
        return [
            'latitude' => $content->getLatitude(),
            'longitude' => $content->getLongitude(),
        ];
    }

    /**
     * @param VenueContract $content
     * @return array
     */
    protected function createFromVenue(VenueContract $content): array
    {
        return [
            'title' => $content->getVenueTitle(),
            'address' => $content->getAddress(),
            'foursquare_id' => $content->getFoursquareId(),
        ];
    }

    /**
     * @param DocumentMessage $document
     * @return array
     * @throws \ReflectionException
     */
    public function createFromDocument(DocumentMessage $document)
    {
        return array_merge($this->createFromSimpleMessage($document), $this->createFromFile($document, 'document'));
    }

    /**
     * @param AudioMessage $audio
     * @return array
     * @throws \ReflectionException
     */
    public function createFromAudio(AudioMessage $audio): array
    {
        return array_merge(
            $this->createFromSimpleMessage($audio),
            $this->createFromFile($audio, 'audio'),
            [
                'duration' => $audio->getDuration(),
                'performer' => $audio->getPerformer(),
                'title' => $audio->getTitle(),
            ]
        );
    }

    /**
     * @param Message $message
     * @return array
     */
    public function createFromTextMessage(Message $message): array
    {
        return array_merge($this->createFromSimpleMessage($message), $this->createFromText($message));
    }

    /**
     * @param TextContract $content
     * @return array
     */
    protected function createFromText(TextContract $content): array
    {
        return [
            'text' => $content->getText(),
            'parse_mode' => $content->getParseMode(),
            'disable_web_page_preview' => $content->getDisableWebPagePreview(),
        ];
    }

    /**
     * @param ManageUserInChatAction $action
     * @return array
     */
    public function createFromUserChatAction(ManageUserInChatAction $action): array
    {
        return array_merge($this->createFromAbstractMessage($action), [
            'user_id' => $action->getUserId(),
        ]);
    }

    /**
     * @param CallbackQueryAnswer $answer
     * @return array
     */
    public function createFromCallbackAnswer(CallbackQueryAnswer $answer): array
    {
        return [
            'callback_query_id' => $answer->getCallbackQueryId(),
            'text' => $answer->getText(),
            'show_alert' => $answer->getShowAlert(),
            'url' => $answer->getUrl(),
        ];
    }

    /**
     * @param InlineResponse $response
     * @return array
     */
    public function createFromInlineResponse(InlineResponse $response): array
    {
        return [
            'inline_query_id' => $response->getInlineQueryId(),
            'results' => $this->createFromInlineResults($response->getResults()),
            'cache_time' => $response->getCacheTime(),
            'is_personal' => $response->isPersonal(),
            'next_offset' => $response->getNextOffset(),
            'switch_pm_text' => $response->getSwitchPmText(),
            'switch_pm_parameter' => $response->getSwitchPmParameter(),
        ];
    }

    /**
     * @param BaseInlineResult[] $results
     * @return array
     */
    protected function createFromInlineResults(array $results): array
    {
        $parsed = [];
        foreach ($results as $result) {
            $parsed[] = $this->transformSingleInlineResult($result);
        }

        return $parsed;
    }

    /**
     * @param BaseInlineResult $result
     * @return array
     */
    protected function transformSingleInlineResult(BaseInlineResult $result): array
    {
        switch (true) {
            case $result instanceof InlineQueryResultArticle:
                return $this->createFromArticleInlineResult($result);
            case $result instanceof InlineQueryResultPhoto:
                return $this->createFromPhotoInlineResult($result);
            case $result instanceof InlineQueryResultGif:
                return $this->createFromGifInlineResult($result);
            case $result instanceof InlineQueryResultMpeg4Gif:
                return $this->createFromMpegInlineResult($result);
            case $result instanceof InlineQueryResultVideo:
                return $this->createFromVideoInlineResult($result);
            case $result instanceof InlineQueryResultAudio:
                return $this->createFromAudioInlineResult($result);
            case $result instanceof InlineQueryResultVoice:
                return $this->createFromVoiceInlineResult($result);
            case $result instanceof InlineQueryResultDocument:
                return $this->createFromDocumentInlineResult($result);
            case $result instanceof InlineQueryResultLocation:
                return $this->createFromLocationInlineResult($result);
            case $result instanceof InlineQueryResultVenue:
                return $this->createFromVenueInlineResult($result);
            case $result instanceof InlineQueryResultContact:
                return $this->createFromContactInlineResult($result);
            default:
                return [];
        }
    }

    /**
     * @param InlineQueryResultArticle $article
     * @return array
     */
    protected function createFromArticleInlineResult(InlineQueryResultArticle $article)
    {
        return array_merge(
            $this->createFromBaseInlineResult($article),
            $this->createReplyMarkUp($article->getReplyMarkUp()),
            $this->createFromThumb($article, 'thumb'),
            [
                'description' => $article->getDescription(),
                'url' => $article->getUrl(),
                'hide_url' => $article->getHideUrl(),
            ]
        );
    }

    /**
     * @param BaseInlineResult $result
     * @return array
     */
    protected function createFromBaseInlineResult(BaseInlineResult $result): array
    {
        return [
            'type' => $result->getType(),
            'id' => $result->getId(),
            'title' => $result->getTitle(),
            'reply_markup' => $this->createReplyMarkUp($result->getReplyMarkUp()),
            'input_message_content' => $this->createInputMessageContent($result->getContent())
        ];
    }

    /**
     * @param $content
     * @return array|null
     */
    protected function createInputMessageContent($content): ?array
    {
        switch (true) {
            case $content instanceof LocationContent:
                return $this->createFromLocation($content);
            case $content instanceof VenueContent:
                return $this->createFromVenue($content);
            case $content instanceof ContactContent:
                return $this->createFromContact($content);
            case $content instanceof TextMessageContent:
                $result = $this->createFromText($content);
                $result['message_text'] = $result['text'] ?? null;
                unset($result['text']);
                return $result;
            default:
                return null;
        }
    }

    /**
     * @param ThumbContract $thumb
     * @param string $prefix
     * @return array
     */
    protected function createFromThumb(ThumbContract $thumb, string $prefix): array
    {
        return [
            $prefix . '_url' => $thumb->getImageUrl(),
            $prefix . '_width' => $thumb->getImageWidth(),
            $prefix . '_height' => $thumb->getImageHeight(),
        ];
    }

    /**
     * @param InlineQueryResultPhoto $photo
     * @return array
     */
    protected function createFromPhotoInlineResult(InlineQueryResultPhoto $photo): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($photo),
            $this->createFromThumb($photo, 'photo'),
            [
                'thumb_url' => $photo->getThumbUrl(),
                'description' => $photo->getDescription(),
                'caption' => $photo->getCaption(),
            ]
        );
    }

    /**
     * @param InlineQueryResultGif $gif
     * @return array
     */
    protected function createFromGifInlineResult(InlineQueryResultGif $gif): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($gif),
            $this->createFromThumb($gif, 'gif'),
            [
                'thumb_url' => $gif->getThumbUrl(),
                'caption' => $gif->getCaption(),
            ]
        );
    }

    /**
     * @param InlineQueryResultMpeg4Gif $video
     * @return array
     */
    protected function createFromMpegInlineResult(InlineQueryResultMpeg4Gif $video): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($video),
            $this->createFromThumb($video, 'mpeg4'),
            [
                'thumb_url' => $video->getThumbUrl(),
                'caption' => $video->getCaption(),
            ]
        );
    }

    /**
     * @param InlineQueryResultVideo $video
     * @return array
     */
    protected function createFromVideoInlineResult(InlineQueryResultVideo $video): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($video),
            $this->createFromThumb($video, 'video'),
            [
                'thumb_url' => $video->getThumbUrl(),
                'description' => $video->getDescription(),
                'caption' => $video->getCaption(),
                'mime_type' => $video->getMime(),
                'video_duration' => $video->getDuration(),
            ]
        );
    }

    /**
     * @param InlineQueryResultAudio $audio
     * @return array
     */
    protected function createFromAudioInlineResult(InlineQueryResultAudio $audio): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($audio),
            [
                'audio_url' => $audio->getAudioUrl(),
                'caption' => $audio->getCaption(),
                'performer' => $audio->getPerformer(),
                'audio_duration' => $audio->getDuration(),
            ]
        );
    }

    /**
     * @param InlineQueryResultVoice $voice
     * @return array
     */
    protected function createFromVoiceInlineResult(InlineQueryResultVoice $voice): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($voice),
            [
                'audio_url' => $voice->getVoiceUrl(),
                'caption' => $voice->getCaption(),
                'voice_duration' => $voice->getDuration(),
            ]
        );
    }

    /**
     * @param InlineQueryResultDocument $document
     * @return array
     */
    protected function createFromDocumentInlineResult(InlineQueryResultDocument $document): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($document),
            $this->createFromThumb($document, 'thumb'),
            [
                'document_url' => $document->getDocumentUrl(),
                'description' => $document->getDescription(),
                'caption' => $document->getCaption(),
                'mime_type' => $document->getMime(),
            ]
        );
    }

    /**
     * @param InlineQueryResultLocation $location
     * @return array
     */
    protected function createFromLocationInlineResult(InlineQueryResultLocation $location): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($location),
            $this->createFromThumb($location, 'thumb'),
            $this->createFromLocation($location)
        );
    }

    /**
     * @param InlineQueryResultVenue $venue
     * @return array
     */
    protected function createFromVenueInlineResult(InlineQueryResultVenue $venue): array
    {
        return array_merge(
            $this->createFromLocationInlineResult($venue),
            $this->createFromVenue($venue)
        );
    }

    /**
     * @param InlineQueryResultContact $contact
     * @return array
     */
    protected function createFromContactInlineResult(InlineQueryResultContact $contact): array
    {
        return array_merge(
            $this->createFromBaseInlineResult($contact),
            $this->createFromThumb($contact, 'thumb'),
            $this->createFromContact($contact)
        );
    }

    /**
     * @param EditMessageCaption $message
     * @return array
     */
    public function createFromEditCaption(EditMessageCaption $message)
    {
        return array_merge(
            $this->createFromSimpleEditMessage($message),
            [
                'chat_id' => $message->getChatId(),
                'caption' => $message->getCaption(),
            ]
        );
    }

    /**
     * @param SimpleEditMessage $message
     * @return array
     */
    protected function createFromSimpleEditMessage(SimpleEditMessage $message): array
    {
        return [
            'message_id' => $message->getMessageId(),
            'inline_message_id' => $message->getInlineMessageId(),
            'reply_markup' => $this->createReplyMarkUp($message->getReplyMarkUp()),
        ];
    }

    /**
     * @param EditMessageText $message
     * @return array
     */
    public function createFromEditText(EditMessageText $message)
    {
        return array_merge(
            $this->createFromSimpleEditMessage($message),
            $this->createFromText($message),
            [
                'chat_id' => $message->getChatId(),
                'text' => $message->getText(),
            ]
        );
    }

    /**
     * @param EditMessageReplyMarkUp $message
     * @return array
     */
    public function createFromEditReplyMarkUp(EditMessageReplyMarkUp $message)
    {
        return array_merge(
            $this->createFromSimpleEditMessage($message),
            [
                'chat_id' => $message->getChatId(),
            ]
        );
    }
}