<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/3/18
 * Time: 10:01 AM
 */

namespace Skipper\Telegram\Factories;

use Skipper\Telegram\Entities\Audio;
use Skipper\Telegram\Entities\Chat;
use Skipper\Telegram\Entities\Contact;
use Skipper\Telegram\Entities\Document;
use Skipper\Telegram\Entities\File;
use Skipper\Telegram\Entities\Message;
use Skipper\Telegram\Entities\Sticker;
use Skipper\Telegram\Entities\User;
use Skipper\Telegram\Entities\Video;
use Skipper\Telegram\Entities\Voice;
use Skipper\Telegram\Requests\InlineRequest;
use Skipper\Telegram\ValueObjects\Location;
use Skipper\Telegram\ValueObjects\MessageEntity;
use Skipper\Telegram\ValueObjects\PhotoSize;
use Skipper\Telegram\ValueObjects\UserProfilePhotos;
use Skipper\Telegram\ValueObjects\Venue;

class ApiEntityFactory
{
    protected const ID = 'id';
    protected const FROM = 'from';
    protected const LOCATION = 'location';
    protected const QUERY = 'query';
    protected const OFFSET = 'offset';
    protected const FIRST_NAME = 'first_name';
    protected const LAST_NAME = 'last_name';
    protected const USERNAME = 'username';
    protected const LATITUDE = 'latitude';
    protected const LONGITUDE = 'longitude';
    protected const MESSAGE_ID = 'message_id';
    protected const DATE = 'date';
    protected const CHAT = 'chat';
    protected const FORWARD_FROM = 'forward_from';
    protected const FORWARD_DATE = 'forward_date';
    protected const REPLY_TO_MESSAGE = 'reply_to_message';
    protected const TEXT = 'text';
    protected const ENTITIES = 'entities';
    protected const AUDIO = 'audio';
    protected const DOCUMENT = 'document';
    protected const STICKER = 'sticker';
    protected const VIDEO = 'video';
    protected const VOICE = 'voice';
    protected const PHOTO = 'photo';
    protected const CAPTION = 'caption';
    protected const VENUE = 'venue';
    protected const NEW_CHAT_MEMBER = 'new_chat_member';
    protected const NEW_CHAT_TITLE = 'new_chat_title';
    protected const NEW_CHAT_PHOTO = 'new_chat_photo';
    protected const DELETE_CHAT_PHOTO = 'delete_chat_photo';
    protected const GROUP_CHAT_CREATED = 'group_chat_created';
    protected const SUPERGROUP_CHAT_CREATED = 'supergroup_chat_created';
    protected const CHANNEL_CHAT_CREATED = 'channel_chat_created';
    protected const MIGRATE_TO_CHAT_ID = 'migrate_to_chat_id';
    protected const MIGRATE_FROM_CHAT_ID = 'migrate_from_chat_id';
    protected const PINNED_MESSAGE = 'pinned_message';
    protected const TYPE = 'type';
    protected const TITLE = 'title';
    protected const ALL_MEMBERS_ARE_ADMINISTRATORS = 'all_members_are_administrators';
    protected const LENGTH = 'length';
    protected const URL = 'url';
    protected const FILE_ID = 'file_id';
    protected const DURATION = 'duration';
    protected const PERFORMER = 'performer';
    protected const MIME_TYPE = 'mime_type';
    protected const FILE_SIZE = 'file_size';
    protected const THUMB = 'thumb';
    protected const FILE_NAME = 'file_name';
    protected const HEIGHT = 'height';
    protected const WIDTH = 'width';
    protected const ADDRESS = 'address';
    protected const TOTAL_COUNT = 'total_count';
    protected const PHOTOS = 'photos';
    protected const FILE_PATH = 'file_path';
    protected const PHONE_NUMBER = 'phone_number';
    protected const USER_ID = 'user_id';

    /**
     * @param array $data
     * @return InlineRequest
     */
    public function createInlineRequest(array $data): InlineRequest
    {
        $request = new InlineRequest;
        $request->setId($data[self::ID])
            ->setFrom($this->createUser($data[self::FROM]))
            ->setLocation($this->createLocation($data[self::LOCATION] ?? []))
            ->setQuery($data[self::QUERY])
            ->setOffset($data[self::OFFSET]);

        return $request;
    }

    /**
     * @param array $data
     * @return null|User
     */
    protected function createUser(array $data): ?User
    {
        if (empty($data)) {
            return null;
        }
        $user = new User();
        $user->setId($data[self::ID])
            ->setFirstName($data[self::FIRST_NAME])
            ->setLastName($data[self::LAST_NAME] ?? null)
            ->setUsername($data[self::USERNAME] ?? null);

        return $user;
    }

    /**
     * @param array $data
     * @return null|Location
     */
    protected function createLocation(array $data): ?Location
    {
        if (empty($data)) {
            return null;
        }
        $location = new Location;
        $location->setLongitude($data[self::LATITUDE])
            ->setLongitude($data[self::LONGITUDE]);

        return $location;
    }

    /**
     * @param array $telegramData
     * @return Message
     * @throws \ReflectionException
     */
    public function createResponseMessage(array $telegramData): Message
    {
        if (empty($telegramData)) {
            return null;
        }
        $message = new Message();
        $message->setMessageId($telegramData[self::MESSAGE_ID])
            ->setFrom($this->createUser($telegramData[self::FROM] ?? []))
            ->setDate($telegramData[self::DATE])
            ->setChat($this->createChat($telegramData[self::CHAT]))
            ->setForwardFrom($this->createUser($telegramData[self::FORWARD_FROM] ?? []))
            ->setForwardDate($telegramData[self::FORWARD_DATE] ?? null)
            ->setReplyToMessage($this->createResponseMessage($telegramData[self::REPLY_TO_MESSAGE] ?? null))
            ->setText($telegramData[self::TEXT]);
        foreach ($telegramData[self::ENTITIES] ?? [] as $entity) {
            $entities[] = $this->createMessageEntity($entity);
        }
        $message->setEntities($entities ?? null)
            ->setAudio($this->createAudio($telegramData[self::AUDIO] ?? []))
            ->setDocument($this->createDocument($telegramData[self::DOCUMENT] ?? []))
            ->setSticker($this->createSticker($telegramData[self::STICKER] ?? []))
            ->setVideo($this->createVideo($telegramData[self::VIDEO] ?? []))
            ->setVoice($this->createVoice($telegramData[self::VOICE] ?? []));
        foreach ($telegramData[self::PHOTO] ?? [] as $item) {
            $photos[] = $this->createPhotoSize($item);
        }
        $message->setPhoto($photos ?? null)
            ->setCaption($telegramData[self::CAPTION] ?? null)
            ->setLocation($this->createLocation($telegramData[self::LOCATION] ?? []))
            ->setVenue($this->createVenue($telegramData[self::VENUE] ?? []))
            ->setNewChatMember($this->createUser($telegramData[self::NEW_CHAT_MEMBER] ?? []))
            ->setNewChatTitle($telegramData[self::NEW_CHAT_TITLE] ?? null);
        foreach ($telegramData[self::NEW_CHAT_PHOTO] as $photo) {
            $chatPhotos[] = $this->createPhotoSize($photo);
        }
        $message->setNewChatPhoto($chatPhotos ?? null)
            ->setDeleteChatPhoto($telegramData[self::DELETE_CHAT_PHOTO] ?? null)
            ->setGroupChatCreated($telegramData[self::GROUP_CHAT_CREATED] ?? null)
            ->setSuperGroupChatCreated($telegramData[self::SUPERGROUP_CHAT_CREATED] ?? null)
            ->setChannelChatCreated($telegramData[self::CHANNEL_CHAT_CREATED] ?? null)
            ->setMigrateToChatId($telegramData[self::MIGRATE_TO_CHAT_ID] ?? null)
            ->setMigrateFromChatId($telegramData[self::MIGRATE_FROM_CHAT_ID] ?? null)
            ->setPinnedMessage($this->createResponseMessage($telegramData[self::PINNED_MESSAGE] ?? []));

        return $message;
    }

    /**
     * @param array $data
     * @return null|Chat
     * @throws \ReflectionException
     */
    protected function createChat(array $data): ?Chat
    {
        if (empty($data)) {
            return null;
        }
        $chat = new Chat;
        $chat->setId($data[self::ID])
            ->setType($data[self::TYPE])
            ->setTitle($data[self::TITLE] ?? null)
            ->setUsername($data[self::USERNAME] ?? null)
            ->setFirstName($data[self::FIRST_NAME] ?? null)
            ->setLastName($data[self::LAST_NAME] ?? null)
            ->setIsAllMembersAreAdministrators($data[self::ALL_MEMBERS_ARE_ADMINISTRATORS] ?? false);

        return $chat;
    }

    /**
     * @param array $data
     * @return null|MessageEntity
     * @throws \ReflectionException
     */
    protected function createMessageEntity(array $data): ?MessageEntity
    {
        if (empty($data)) {
            return null;
        }
        $entity = new MessageEntity($data[self::TYPE]);
        $entity->setLength($data[self::LENGTH])
            ->setOffset($data[self::OFFSET])
            ->setUrl($data[self::URL] ?? null);

        return $entity;
    }

    /**
     * @param array $data
     * @return null|Audio
     */
    protected function createAudio(array $data): ?Audio
    {
        if (empty($data)) {
            return null;
        }
        $audio = new Audio;
        $audio->setFileId($data[self::FILE_ID])
            ->setDuration($data[self::DURATION])
            ->setPerformer($data[self::PERFORMER] ?? null)
            ->setTitle($data[self::TITLE])
            ->setMimeType($data[self::MIME_TYPE] ?? null)
            ->setFileSize($data[self::FILE_SIZE] ?? null);

        return $audio;
    }

    /**
     * @param array $data
     * @return null|Document
     */
    protected function createDocument(array $data): ?Document
    {
        if (empty($data)) {
            return null;
        }

        $document = new Document;
        $document->setFileId($data[self::FILE_ID])
            ->setFileSize($data[self::FILE_SIZE] ?? null)
            ->setMimeType($data[self::MIME_TYPE] ?? null)
            ->setThumb($this->createPhotoSize($data[self::THUMB] ?? []))
            ->setFileName($data[self::FILE_NAME]);

        return $document;
    }

    /**
     * @param array $data
     * @return null|PhotoSize
     */
    protected function createPhotoSize(array $data): ?PhotoSize
    {
        if (empty($data)) {
            return null;
        }
        $photo = new PhotoSize;
        $photo->setFileId($data[self::FILE_ID])
            ->setFileSize($data[self::FILE_SIZE] ?? null)
            ->setHeight($data[self::HEIGHT])
            ->setWidth($data[self::WIDTH]);

        return $photo;
    }

    /**
     * @param array $data
     * @return null|Sticker
     */
    protected function createSticker(array $data): ?Sticker
    {
        if (empty($data)) {
            return null;
        }

        $sticker = new Sticker;
        $sticker->setFileId($data[self::FILE_ID])
            ->setWidth($data[self::WIDTH])
            ->setHeight($data[self::HEIGHT])
            ->setThumb($this->createPhotoSize($data[self::THUMB] ?? []))
            ->setFileSize($data[self::FILE_SIZE] ?? null);

        return $sticker;
    }

    /**
     * @param array $data
     * @return null|Video
     */
    protected function createVideo(array $data): ?Video
    {
        if (empty($data)) {
            return null;
        }
        $video = new Video;
        $video->setFileId($data[self::FILE_ID])
            ->setFileSize($data[self::FILE_SIZE] ?? null)
            ->setWidth($data[self::WIDTH])
            ->setHeight($data[self::HEIGHT])
            ->setDuration($data[self::DURATION])
            ->setThumb($this->createPhotoSize($data[self::THUMB] ?? []))
            ->setMimeType($data[self::MIME_TYPE]);

        return $video;
    }

    /**
     * @param array $data
     * @return null|Voice
     */
    protected function createVoice(array $data): ?Voice
    {
        if (empty($data)) {
            return null;
        }
        $voice = new Voice;
        $voice->setFileId($data[self::FILE_ID])
            ->setDuration($data[self::DURATION])
            ->setMimeType($data[self::MIME_TYPE] ?? null)
            ->setFileSize($data[self::FILE_SIZE] ?? null);

        return $voice;
    }

    /**
     * @param array $data
     * @return null|Venue
     */
    protected function createVenue(array $data): ?Venue
    {
        if (empty($data)) {
            return null;
        }
        $venue = new Venue;
        $venue->setLocation($this->createLocation($data[self::LOCATION]))
            ->setTitle($data[self::TITLE])
            ->setAddress($data[self::ADDRESS]);

        return $venue;
    }

    /**
     * @param string $methodName
     * @param array $responseData
     * @return mixed
     */
    public function manageResponse(string $methodName, array $responseData)
    {
        switch ($methodName) {
            case 'getMe':
                return $this->createUser($responseData);
            case 'getUserProfilePhotos':
                return $this->createUserProfilePhoto($responseData);
            case 'kickAndBanFromChat':
            case 'unbanUserInChat':
            case 'answerToCallback':
            case 'downloadFile':
            case 'getFile':
                return reset($responseData);
            default:
                return $responseData;
        }
    }

    /**
     * @param array $data
     * @return null|UserProfilePhotos
     */
    protected function createUserProfilePhoto(array $data): ?UserProfilePhotos
    {
        if (empty($data)) {
            return null;
        }
        $photos = new UserProfilePhotos;
        $photos->setTotalCount($data[self::TOTAL_COUNT]);
        foreach ($data[self::PHOTOS] as $photo) {
            $p[] = $this->createPhotoSize($photo);
        }
        $photos->setPhotos($p ?? []);

        return $photos;
    }

    /**
     * @param array $data
     * @return null|File
     */
    protected function createFile(array $data): ?File
    {
        if (empty($data)) {
            return null;
        }
        $file = new File;
        $file->setFileId($data[self::FILE_ID])
            ->setFilePath($data[self::FILE_PATH] ?? null)
            ->setFileSize($data[self::FILE_SIZE] ?? null);

        return $file;
    }

    /**
     * @param array $data
     * @return null|Contact
     */
    protected function createContact(array $data): ?Contact
    {
        if (empty($data)) {
            return null;
        }
        $contact = new Contact;
        $contact->setPhoneNumber($data[self::PHONE_NUMBER])
            ->setFirstName($data[self::FIRST_NAME])
            ->setLastName($data[self::LAST_NAME] ?? null)
            ->setUserId($data[self::USER_ID] ?? null);

        return $contact;
    }
}