<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 3:10 PM
 */

namespace Skipper\Telegram\Enumerators;

class ParseModeEnumerator extends AbstractEnumerator
{
    public const MARKDOWN = 'Markdown';
    public const HTML = 'HTML';
}