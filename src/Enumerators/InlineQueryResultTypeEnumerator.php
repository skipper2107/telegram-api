<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 12:28 PM
 */

namespace Skipper\Telegram\Enumerators;

class InlineQueryResultTypeEnumerator extends AbstractEnumerator
{
    public const AUDIO = 'audio';
    public const ARTICLE = 'article';
    public const PHOTO = 'photo';
    public const GIF = 'gif';
    public const MPEG4_GIF = 'mpeg4_gif';
    public const VIDEO = 'video';
    public const VOICE = 'voice';
    public const DOCUMENT = 'document';
    public const LOCATION = 'location';
    public const VENUE = 'venue';
    public const CONTACT = 'contact';
}