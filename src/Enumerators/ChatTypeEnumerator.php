<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 1:24 PM
 */

namespace Skipper\Telegram\Enumerators;

class ChatTypeEnumerator extends AbstractEnumerator
{
    public const PRIVATE_TYPE = 'private';
    public const GROUP_TYPE = 'group';
    public const SUPERGROUP_TYPE = 'supergroup';
    public const CHANNEL_TYPE = 'channel';
}