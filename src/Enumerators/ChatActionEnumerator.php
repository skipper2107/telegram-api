<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 12:19 PM
 */

namespace Skipper\Telegram\Enumerators;

class ChatActionEnumerator extends AbstractEnumerator
{
    public const TYPING = 'typing';
    public const UPLOAD_PHOTO = 'upload_photo';
    public const UPLOAD_VIDEO = 'upload_video';
    public const RECORD_VIDEO = 'record_video';
    public const RECORD_AUDIO = 'record_audio';
    public const UPLOAD_AUDIO = 'upload_audio';
    public const UPLOAD_DOCUMENT = 'upload_document';
    public const FIND_LOCATION = 'find_location';
}