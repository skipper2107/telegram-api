<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:47
 */

namespace Skipper\Telegram\Enumerators;

use Skipper\Telegram\Exceptions\BadParameterException;

abstract class AbstractEnumerator
{
    /**
     * @param mixed $value
     * @throws BadParameterException
     * @throws \ReflectionException
     */
    public function assertValidValue($value)
    {
        if (false === in_array($value, $this->getAllValues())) {
            throw new BadParameterException('Invalid Enumerator Value:' . $value, [
                'value' => $value,
            ]);
        }
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getAllValues(): array
    {
        return (new \ReflectionClass(static::class))->getConstants();
    }

    /**
     * @param string|int|double $value
     * @return string|int|double
     * @throws \ReflectionException
     */
    public function normalize($value)
    {
        $values = $this->getAllValues();
        $value = mb_strtolower($value);
        foreach ($values as $key => $item) {
            if (mb_strtolower($item) === $value) {
                return $item;
            }
        }

        return reset($values);
    }
}