<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:28 AM
 */

namespace Skipper\Telegram\Enumerators;

class MimeTypeEnumerator extends AbstractEnumerator
{
    public const HTML = 'text/html';
    public const VIDEO = 'video/mp4';
    public const PDF = 'application/pdf';
    public const ZIP = 'application/zip';
}