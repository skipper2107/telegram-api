<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:46
 */

namespace Skipper\Telegram\Enumerators;

class MessageEntityEnumerator extends AbstractEnumerator
{
    public const MENTION = 'mention';
    public const HASHTAG = 'hashtag';
    public const COMMAND = 'bot_command';
    public const URL = 'url';
    public const EMAIL = 'email';
    public const BOLD = 'bold';
    public const ITALIC = 'italic';
    public const CODE = 'code';
    public const PRE = 'pre';
    public const TEXT_LINK = 'text_link';
}