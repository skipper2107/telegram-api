<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 4:45 PM
 */

namespace Skipper\Telegram;

use Skipper\Telegram\Entities\Message as MessageEntity;
use Skipper\Telegram\Entities\User;
use Skipper\Telegram\Exceptions\ApiException;
use Skipper\Telegram\Exceptions\BadParameterException;
use Skipper\Telegram\Factories\ApiEntityFactory;
use Skipper\Telegram\Sendable\AbstractMessage;
use Skipper\Telegram\Sendable\AudioMessage;
use Skipper\Telegram\Sendable\CallbackQueryAnswer;
use Skipper\Telegram\Sendable\ChatActionMessage;
use Skipper\Telegram\Sendable\ContactMessage;
use Skipper\Telegram\Sendable\DocumentMessage;
use Skipper\Telegram\Sendable\EditMessageCaption;
use Skipper\Telegram\Sendable\EditMessageReplyMarkUp;
use Skipper\Telegram\Sendable\EditMessageText;
use Skipper\Telegram\Sendable\ForwardMessage;
use Skipper\Telegram\Sendable\InlineResponse;
use Skipper\Telegram\Sendable\LocationMessage;
use Skipper\Telegram\Sendable\ManageUserInChatAction;
use Skipper\Telegram\Sendable\Message;
use Skipper\Telegram\Sendable\PhotoMessage;
use Skipper\Telegram\Sendable\SimpleEditMessage;
use Skipper\Telegram\Sendable\StickerMessage;
use Skipper\Telegram\Sendable\VenueMessage;
use Skipper\Telegram\Sendable\VideoMessage;
use Skipper\Telegram\Sendable\VoiceMessage;
use Skipper\Telegram\ValueObjects\UserProfilePhotos;

/**
 * Class Telegram
 * @package Skipper\Telegram
 * @method User getMe()
 * @method string getFile(string $fileId)
 * @method bool answerToCallback(CallbackQueryAnswer $answer)
 * @method bool kickAndBanFromChat(ManageUserInChatAction $action)
 * @method bool unbanUserInChat(ManageUserInChatAction $action)
 * @method string downloadFile(string $locationPath, string $savePath)
 * @method UserProfilePhotos getUserProfilePhotos(int $userId, ?int $offset = null, ?int $limit = null)
 */
class Telegram
{
    /**
     * @var Client $client
     */
    protected $client;

    /**
     * @var ApiEntityFactory
     */
    protected $responseTransformer;

    public function __construct(Client $client, ApiEntityFactory $responseTransformer)
    {
        $this->client = $client;
        $this->responseTransformer = $responseTransformer;
    }

    /**
     * @param InlineResponse $response
     * @return bool
     * @throws ApiException
     */
    public function sendInline(InlineResponse $response): bool
    {
        return $this->client->sendInlineResult($response);
    }

    /**
     * @param SimpleEditMessage $editMessage
     * @return MessageEntity
     * @throws ApiException
     * @throws BadParameterException
     * @throws \ReflectionException
     */
    public function edit(SimpleEditMessage $editMessage): MessageEntity
    {
        switch (true) {
            case $editMessage instanceof EditMessageText:
                $response = $this->client->editMessageText($editMessage);
                break;
            case $editMessage instanceof EditMessageCaption:
                $response = $this->client->editMessageCaption($editMessage);
                break;
            case $editMessage instanceof EditMessageReplyMarkUp:
                $response = $this->client->editMessageReplyMarkUp($editMessage);
                break;
            default:
                throw new BadParameterException('Invalid edit message given', [
                    'message' => get_class($editMessage),
                ]);
        }
        return $this->responseTransformer->createResponseMessage($response);
    }

    /**
     * @param AbstractMessage $message
     * @return MessageEntity
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function send(AbstractMessage $message): MessageEntity
    {
        switch (true) {
            case $message instanceof Message:
                $response = $this->client->sendMessage($message);
                break;
            case $message instanceof ContactMessage:
                $response = $this->client->sendContact($message);
                break;
            case $message instanceof PhotoMessage:
                $response = $this->client->sendPhoto($message);
                break;
            case $message instanceof DocumentMessage:
                $response = $this->client->sendDocument($message);
                break;
            case $message instanceof VideoMessage:
                $response = $this->client->sendVideo($message);
                break;
            case $message instanceof AudioMessage:
                $response = $this->client->sendAudio($message);
                break;
            case $message instanceof ChatActionMessage:
                $response = $this->client->sendChatAction($message);
                break;
            case $message instanceof VoiceMessage:
                $response = $this->client->sendVoice($message);
                break;
            case $message instanceof StickerMessage:
                $response = $this->client->sendSticker($message);
                break;
            case $message instanceof VenueMessage:
                $response = $this->client->sendVenue($message);
                break;
            case $message instanceof LocationMessage:
                $response = $this->client->sendLocation($message);
                break;
            case $message instanceof ForwardMessage:
                $response = $this->client->forwardMessage($message);
                break;
            default:
                throw new BadParameterException('Invalid message given', [
                    'message' => get_class($message),
                ]);
        }

        return $this->responseTransformer->createResponseMessage($response);
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws BadParameterException
     */
    public function __call(string $name, array $arguments)
    {
        if (false === method_exists($this->client, $name)) {
            throw new BadParameterException('UnknownApiMethod:' . $name, [
                'method_name' => $name,
                'args' => $arguments,
            ]);
        }

        return $this->responseTransformer->manageResponse($name, (array)$this->client->{$name}(...$arguments));
    }
}