<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:16 PM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Contracts\VenueContract;

trait HasVenue
{
    /** @var $title string */
    protected $title;
    /** @var $address string */
    protected $address;
    /** @var $foursquareId string|null */
    protected $foursquareId;

    /**
     * @return string
     */
    public function getVenueTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VenueContract
     */
    public function setVenueTitle(string $title): VenueContract
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return VenueContract
     */
    public function setAddress(string $address): VenueContract
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFoursquareId(): ?string
    {
        return $this->foursquareId;
    }

    /**
     * @param null|string $foursquareId
     * @return VenueContract
     */
    public function setFoursquareId(?string $foursquareId): VenueContract
    {
        $this->foursquareId = $foursquareId;

        return $this;
    }
}