<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:20 PM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Contracts\ContactContract;

trait HasContact
{
    /** @var $phoneNumber string */
    protected $phoneNumber;
    /** @var $firstName string */
    protected $firstName;
    /** @var $lastName string|null */
    protected $lastName;

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return ContactContract
     */
    public function setPhoneNumber(string $phoneNumber): ContactContract
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return ContactContract
     */
    public function setFirstName(string $firstName): ContactContract
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     * @return ContactContract
     */
    public function setLastName(?string $lastName): ContactContract
    {
        $this->lastName = $lastName;

        return $this;
    }
}