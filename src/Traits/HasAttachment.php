<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 11:49 AM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Sendable\Attachment;

trait HasAttachment
{
    /** @var $file Attachment|null */
    protected $file;
    /** @var $caption string|null */
    protected $caption;

    /**
     * @return null|Attachment
     */
    public function getFile(): ?Attachment
    {
        return $this->file;
    }

    /**
     * @param null|Attachment $file
     * @return HasAttachment
     */
    public function setFile(?Attachment $file): HasAttachment
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return HasAttachment
     */
    public function setCaption(?string $caption): HasAttachment
    {
        $this->caption = $caption;
        return $this;
    }
}