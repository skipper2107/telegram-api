<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:59 PM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Contracts\ThumbContract;

trait HasSize
{
    /**
     * @var string|null
     */
    protected $imageUrl;

    /**
     * @var integer|null
     */
    protected $imageWidth;

    /**
     * @var integer|null
     */
    protected $imageHeight;

    /**
     * @return null|string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param null|string $imageUrl
     * @return ThumbContract
     */
    public function setImageUrl(?string $imageUrl): ThumbContract
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getImageWidth(): ?int
    {
        return $this->imageWidth;
    }

    /**
     * @param int|null $imageWidth
     * @return ThumbContract
     */
    public function setImageWidth(?int $imageWidth): ThumbContract
    {
        $this->imageWidth = $imageWidth;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getImageHeight(): ?int
    {
        return $this->imageHeight;
    }

    /**
     * @param int|null $imageHeight
     * @return ThumbContract
     */
    public function setImageHeight(?int $imageHeight): ThumbContract
    {
        $this->imageHeight = $imageHeight;
        return $this;
    }


}