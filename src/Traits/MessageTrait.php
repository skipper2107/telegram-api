<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:13 AM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Contracts\TextContract;
use Skipper\Telegram\Enumerators\ParseModeEnumerator;

trait MessageTrait
{
    /** @var $text string */
    protected $text;
    /** @var $parseMode string|null */
    protected $parseMode;
    /** @var $disableWebPagePreview bool|null */
    protected $disableWebPagePreview;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return TextContract
     */
    public function setText(string $text): TextContract
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getParseMode(): ?string
    {
        return $this->parseMode;
    }

    /**
     * @param null|string $parseMode
     * @return TextContract
     * @throws \ReflectionException
     */
    public function setParseMode(?string $parseMode): TextContract
    {
        $this->parseMode = (new ParseModeEnumerator)->normalize($parseMode);
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDisableWebPagePreview(): ?bool
    {
        return $this->disableWebPagePreview;
    }

    /**
     * @param bool|null $disableWebPagePreview
     * @return TextContract
     */
    public function setDisableWebPagePreview(?bool $disableWebPagePreview): TextContract
    {
        $this->disableWebPagePreview = $disableWebPagePreview;
        return $this;
    }
}