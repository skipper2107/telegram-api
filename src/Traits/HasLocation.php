<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:12 PM
 */

namespace Skipper\Telegram\Traits;

use Skipper\Telegram\Contracts\LocationContract;

trait HasLocation
{
    /** @var $latitude double */
    protected $latitude;
    /** @var $longitude double */
    protected $longitude;

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     * @return LocationContract
     */
    public function setLatitude(float $latitude): LocationContract
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     * @return LocationContract
     */
    public function setLongitude(float $longitude): LocationContract
    {
        $this->longitude = $longitude;

        return $this;
    }
}