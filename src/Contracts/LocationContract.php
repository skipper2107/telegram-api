<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 10:36 AM
 */

namespace Skipper\Telegram\Contracts;

interface LocationContract
{
    /**
     * @return float
     */
    public function getLatitude(): float;

    /**
     * @param float $latitude
     * @return LocationContract
     */
    public function setLatitude(float $latitude): LocationContract;

    /**
     * @return float
     */
    public function getLongitude(): float;

    /**
     * @param float $longitude
     * @return LocationContract
     */
    public function setLongitude(float $longitude): LocationContract;
}