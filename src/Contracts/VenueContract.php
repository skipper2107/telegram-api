<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 10:38 AM
 */

namespace Skipper\Telegram\Contracts;

interface VenueContract
{
    /**
     * @return string
     */
    public function getVenueTitle(): string;

    /**
     * @param string $title
     * @return VenueContract
     */
    public function setVenueTitle(string $title): VenueContract;

    /**
     * @return string
     */
    public function getAddress(): string;

    /**
     * @param string $address
     * @return VenueContract
     */
    public function setAddress(string $address): VenueContract;

    /**
     * @return null|string
     */
    public function getFoursquareId(): ?string;

    /**
     * @param null|string $foursquareId
     * @return VenueContract
     */
    public function setFoursquareId(?string $foursquareId): VenueContract;
}