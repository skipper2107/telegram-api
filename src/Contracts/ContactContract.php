<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 10:37 AM
 */

namespace Skipper\Telegram\Contracts;

interface ContactContract
{
    /**
     * @return string
     */
    public function getPhoneNumber(): string;

    /**
     * @param string $phoneNumber
     * @return ContactContract
     */
    public function setPhoneNumber(string $phoneNumber): ContactContract;

    /**
     * @return string
     */
    public function getFirstName(): string;

    /**
     * @param string $firstName
     * @return ContactContract
     */
    public function setFirstName(string $firstName): ContactContract;

    /**
     * @return null|string
     */
    public function getLastName(): ?string;

    /**
     * @param null|string $lastName
     * @return ContactContract
     */
    public function setLastName(?string $lastName): ContactContract;
}