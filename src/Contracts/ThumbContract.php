<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 11:06 AM
 */

namespace Skipper\Telegram\Contracts;

interface ThumbContract
{
    /**
     * @return null|string
     */
    public function getImageUrl(): ?string;

    /**
     * @param null|string $imageUrl
     * @return ThumbContract
     */
    public function setImageUrl(?string $imageUrl): ThumbContract;

    /**
     * @return int|null
     */
    public function getImageWidth(): ?int;

    /**
     * @param int|null $imageWidth
     * @return ThumbContract
     */
    public function setImageWidth(?int $imageWidth): ThumbContract;

    /**
     * @return int|null
     */
    public function getImageHeight(): ?int;

    /**
     * @param int|null $imageHeight
     * @return ThumbContract
     */
    public function setImageHeight(?int $imageHeight): ThumbContract;
}