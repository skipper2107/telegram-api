<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 10:56 AM
 */

namespace Skipper\Telegram\Contracts;

interface TextContract
{
    /**
     * @return string
     */
    public function getText(): string;

    /**
     * @param string $text
     * @return TextContract
     */
    public function setText(string $text): TextContract;

    /**
     * @return null|string
     */
    public function getParseMode(): ?string;

    /**
     * @param null|string $parseMode
     * @return TextContract
     */
    public function setParseMode(?string $parseMode): TextContract;

    /**
     * @return bool|null
     */
    public function getDisableWebPagePreview(): ?bool;

    /**
     * @param bool|null $disableWebPagePreview
     * @return TextContract
     */
    public function setDisableWebPagePreview(?bool $disableWebPagePreview): TextContract;
}