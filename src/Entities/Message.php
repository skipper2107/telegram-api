<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:39
 */

namespace Skipper\Telegram\Entities;

use Skipper\Telegram\ValueObjects\Location;
use Skipper\Telegram\ValueObjects\MessageEntity;
use Skipper\Telegram\ValueObjects\PhotoSize;
use Skipper\Telegram\ValueObjects\Venue;

class Message
{
    /** @var $messageId int */
    protected $messageId;
    /** @var $from User|null */
    protected $from;
    /** @var $date int */
    protected $date;
    /** @var $chat Chat */
    protected $chat;
    /** @var $forwardFrom User|null */
    protected $forwardFrom;
    /** @var $forwardDate int|null */
    protected $forwardDate;
    /** @var $replyToMessage Message|null */
    protected $replyToMessage;
    /** @var $text string|null */
    protected $text;
    /** @var $entities MessageEntity[]|null */
    protected $entities;
    /** @var $audio Audio|null */
    protected $audio;
    /** @var $document Document|null */
    protected $document;
    /** @var $photo PhotoSize[]|null */
    protected $photo;
    /** @var $sticker Sticker|null */
    protected $sticker;
    /** @var $video Video|null */
    protected $video;
    /** @var $voice Voice|null */
    protected $voice;
    /** @var $caption string|null */
    protected $caption;
    /** @var $contact Contact|null */
    protected $contact;
    /** @var $location Location|null */
    protected $location;
    /** @var $venue Venue|null */
    protected $venue;
    /** @var $newChatMember User|null */
    protected $newChatMember;
    /** @var $leftChatMember User */
    protected $leftChatMember;
    /** @var $newChatTitle string|null */
    protected $newChatTitle;
    /** @var $newChatPhoto PhotoSize[]|null */
    protected $newChatPhoto;
    /** @var $deleteChatPhoto bool|null */
    protected $deleteChatPhoto;
    /** @var $groupChatCreated bool|null */
    protected $groupChatCreated;
    /** @var $superGroupChatCreated bool|null */
    protected $superGroupChatCreated;
    /** @var $channelChatCreated bool|null */
    protected $channelChatCreated;
    /** @var $migrateToChatId int|null */
    protected $migrateToChatId;
    /** @var $migrateFromChatId int|null */
    protected $migrateFromChatId;
    /** @var $pinnedMessage Message|null */
    protected $pinnedMessage;

    /**
     * @return int
     */
    public function getMessageId(): int
    {
        return $this->messageId;
    }

    /**
     * @param int $messageId
     * @return Message
     */
    public function setMessageId(int $messageId): Message
    {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @return null|User
     */
    public function getFrom(): ?User
    {
        return $this->from;
    }

    /**
     * @param null|User $from
     * @return Message
     */
    public function setFrom(?User $from): Message
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @param int $date
     * @return Message
     */
    public function setDate(int $date): Message
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Chat
     */
    public function getChat(): Chat
    {
        return $this->chat;
    }

    /**
     * @param Chat $chat
     * @return Message
     */
    public function setChat(Chat $chat): Message
    {
        $this->chat = $chat;
        return $this;
    }

    /**
     * @return null|User
     */
    public function getForwardFrom(): ?User
    {
        return $this->forwardFrom;
    }

    /**
     * @param null|User $forwardFrom
     * @return Message
     */
    public function setForwardFrom(?User $forwardFrom): Message
    {
        $this->forwardFrom = $forwardFrom;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getForwardDate(): ?int
    {
        return $this->forwardDate;
    }

    /**
     * @param int|null $forwardDate
     * @return Message
     */
    public function setForwardDate(?int $forwardDate): Message
    {
        $this->forwardDate = $forwardDate;
        return $this;
    }

    /**
     * @return null|Message
     */
    public function getReplyToMessage(): ?Message
    {
        return $this->replyToMessage;
    }

    /**
     * @param null|Message $replyToMessage
     * @return Message
     */
    public function setReplyToMessage(?Message $replyToMessage): Message
    {
        $this->replyToMessage = $replyToMessage;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param null|string $text
     * @return Message
     */
    public function setText(?string $text): Message
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return null|MessageEntity[]
     */
    public function getEntities(): ?array
    {
        return $this->entities;
    }

    /**
     * @param null|MessageEntity[] $entities
     * @return Message
     */
    public function setEntities(?array $entities): Message
    {
        $this->entities = $entities;
        return $this;
    }

    /**
     * @return null|Audio
     */
    public function getAudio(): ?Audio
    {
        return $this->audio;
    }

    /**
     * @param null|Audio $audio
     * @return Message
     */
    public function setAudio(?Audio $audio): Message
    {
        $this->audio = $audio;
        return $this;
    }

    /**
     * @return null|Document
     */
    public function getDocument(): ?Document
    {
        return $this->document;
    }

    /**
     * @param null|Document $document
     * @return Message
     */
    public function setDocument(?Document $document): Message
    {
        $this->document = $document;
        return $this;
    }

    /**
     * @return null|PhotoSize[]
     */
    public function getPhoto(): ?array
    {
        return $this->photo;
    }

    /**
     * @param null|PhotoSize[] $photo
     * @return Message
     */
    public function setPhoto(?array $photo): Message
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return null|Sticker
     */
    public function getSticker(): ?Sticker
    {
        return $this->sticker;
    }

    /**
     * @param null|Sticker $sticker
     * @return Message
     */
    public function setSticker(?Sticker $sticker): Message
    {
        $this->sticker = $sticker;
        return $this;
    }

    /**
     * @return null|Video
     */
    public function getVideo(): ?Video
    {
        return $this->video;
    }

    /**
     * @param null|Video $video
     * @return Message
     */
    public function setVideo(?Video $video): Message
    {
        $this->video = $video;
        return $this;
    }

    /**
     * @return null|Voice
     */
    public function getVoice(): ?Voice
    {
        return $this->voice;
    }

    /**
     * @param null|Voice $voice
     * @return Message
     */
    public function setVoice(?Voice $voice): Message
    {
        $this->voice = $voice;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return Message
     */
    public function setCaption(?string $caption): Message
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return null|Contact
     */
    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    /**
     * @param null|Contact $contact
     * @return Message
     */
    public function setContact(?Contact $contact): Message
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return null|Location
     */
    public function getLocation(): ?Location
    {
        return $this->location;
    }

    /**
     * @param null|Location $location
     * @return Message
     */
    public function setLocation(?Location $location): Message
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return null|Venue
     */
    public function getVenue(): ?Venue
    {
        return $this->venue;
    }

    /**
     * @param null|Venue $venue
     * @return Message
     */
    public function setVenue(?Venue $venue): Message
    {
        $this->venue = $venue;
        return $this;
    }

    /**
     * @return null|User
     */
    public function getNewChatMember(): ?User
    {
        return $this->newChatMember;
    }

    /**
     * @param null|User $newChatMember
     * @return Message
     */
    public function setNewChatMember(?User $newChatMember): Message
    {
        $this->newChatMember = $newChatMember;
        return $this;
    }

    /**
     * @return User
     */
    public function getLeftChatMember(): User
    {
        return $this->leftChatMember;
    }

    /**
     * @param User $leftChatMember
     * @return Message
     */
    public function setLeftChatMember(User $leftChatMember): Message
    {
        $this->leftChatMember = $leftChatMember;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNewChatTitle(): ?string
    {
        return $this->newChatTitle;
    }

    /**
     * @param null|string $newChatTitle
     * @return Message
     */
    public function setNewChatTitle(?string $newChatTitle): Message
    {
        $this->newChatTitle = $newChatTitle;
        return $this;
    }

    /**
     * @return null|PhotoSize[]
     */
    public function getNewChatPhoto(): ?array
    {
        return $this->newChatPhoto;
    }

    /**
     * @param null|PhotoSize[] $newChatPhoto
     * @return Message
     */
    public function setNewChatPhoto(?array $newChatPhoto): Message
    {
        $this->newChatPhoto = $newChatPhoto;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeleteChatPhoto(): ?bool
    {
        return $this->deleteChatPhoto;
    }

    /**
     * @param bool|null $deleteChatPhoto
     * @return Message
     */
    public function setDeleteChatPhoto(?bool $deleteChatPhoto): Message
    {
        $this->deleteChatPhoto = $deleteChatPhoto;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getGroupChatCreated(): ?bool
    {
        return $this->groupChatCreated;
    }

    /**
     * @param bool|null $groupChatCreated
     * @return Message
     */
    public function setGroupChatCreated(?bool $groupChatCreated): Message
    {
        $this->groupChatCreated = $groupChatCreated;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSuperGroupChatCreated(): ?bool
    {
        return $this->superGroupChatCreated;
    }

    /**
     * @param bool|null $superGroupChatCreated
     * @return Message
     */
    public function setSuperGroupChatCreated(?bool $superGroupChatCreated): Message
    {
        $this->superGroupChatCreated = $superGroupChatCreated;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getChannelChatCreated(): ?bool
    {
        return $this->channelChatCreated;
    }

    /**
     * @param bool|null $channelChatCreated
     * @return Message
     */
    public function setChannelChatCreated(?bool $channelChatCreated): Message
    {
        $this->channelChatCreated = $channelChatCreated;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMigrateToChatId(): ?int
    {
        return $this->migrateToChatId;
    }

    /**
     * @param int|null $migrateToChatId
     * @return Message
     */
    public function setMigrateToChatId(?int $migrateToChatId): Message
    {
        $this->migrateToChatId = $migrateToChatId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMigrateFromChatId(): ?int
    {
        return $this->migrateFromChatId;
    }

    /**
     * @param int|null $migrateFromChatId
     * @return Message
     */
    public function setMigrateFromChatId(?int $migrateFromChatId): Message
    {
        $this->migrateFromChatId = $migrateFromChatId;
        return $this;
    }

    /**
     * @return null|Message
     */
    public function getPinnedMessage(): ?Message
    {
        return $this->pinnedMessage;
    }

    /**
     * @param null|Message $pinnedMessage
     * @return Message
     */
    public function setPinnedMessage(?Message $pinnedMessage): Message
    {
        $this->pinnedMessage = $pinnedMessage;
        return $this;
    }

}