<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 4:31 PM
 */

namespace Skipper\Telegram\Entities;

use Skipper\Telegram\Enumerators\ChatTypeEnumerator;

class Chat
{
    /** @var $id int */
    protected $id;
    /** @var $type string */
    protected $type;
    /** @var $title string|null */
    protected $title;
    /** @var $username string|null */
    protected $username;
    /** @var $firstName string|null */
    protected $firstName;
    /** @var $lastName string|null */
    protected $lastName;
    /** @var $isAllMembersAreAdministrators bool */
    protected $isAllMembersAreAdministrators = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Chat
     */
    public function setId(int $id): Chat
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Chat
     * @throws \ReflectionException
     */
    public function setType(string $type): Chat
    {
        $this->type = (new ChatTypeEnumerator)->normalize($type);
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return Chat
     */
    public function setTitle(?string $title): Chat
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param null|string $username
     * @return Chat
     */
    public function setUsername(?string $username): Chat
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param null|string $firstName
     * @return Chat
     */
    public function setFirstName(?string $firstName): Chat
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param null|string $lastName
     * @return Chat
     */
    public function setLastName(?string $lastName): Chat
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllMembersAreAdministrators(): bool
    {
        return $this->isAllMembersAreAdministrators;
    }

    /**
     * @param bool $isAllMembersAreAdministrators
     * @return Chat
     */
    public function setIsAllMembersAreAdministrators(bool $isAllMembersAreAdministrators): Chat
    {
        $this->isAllMembersAreAdministrators = $isAllMembersAreAdministrators;
        return $this;
    }

}