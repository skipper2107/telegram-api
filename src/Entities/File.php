<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 16:31
 */

namespace Skipper\Telegram\Entities;

class File
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $fileSize int|null */
    protected $fileSize;
    /** @var $filePath string|null */
    protected $filePath;

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return File
     */
    public function setFileId(string $fileId): File
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return File
     */
    public function setFileSize(?int $fileSize): File
    {
        $this->fileSize = $fileSize;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param null|string $filePath
     * @return File
     */
    public function setFilePath(?string $filePath): File
    {
        $this->filePath = $filePath;
        return $this;
    }
}