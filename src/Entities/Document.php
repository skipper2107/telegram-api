<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:19
 */

namespace Skipper\Telegram\Entities;

use Skipper\Telegram\ValueObjects\PhotoSize;

class Document
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $thumb PhotoSize|null */
    protected $thumb;
    /** @var $fileName string|null */
    protected $fileName;
    /** @var $mimeType string|null */
    protected $mimeType;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return Document
     */
    public function setFileId(string $fileId): Document
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return null|PhotoSize
     */
    public function getThumb(): ?PhotoSize
    {
        return $this->thumb;
    }

    /**
     * @param null|PhotoSize $thumb
     * @return Document
     */
    public function setThumb(?PhotoSize $thumb): Document
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param null|string $fileName
     * @return Document
     */
    public function setFileName(?string $fileName): Document
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param null|string $mimeType
     * @return Document
     */
    public function setMimeType(?string $mimeType): Document
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return Document
     */
    public function setFileSize(?int $fileSize): Document
    {
        $this->fileSize = $fileSize;
        return $this;
    }
}