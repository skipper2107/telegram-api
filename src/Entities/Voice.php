<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:45
 */

namespace Skipper\Telegram\Entities;

class Voice
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $duration int */
    protected $duration;
    /** @var $mimeType string|null */
    protected $mimeType;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return Voice
     */
    public function setFileId(string $fileId): Voice
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return Voice
     */
    public function setDuration(int $duration): Voice
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param null|string $mimeType
     * @return Voice
     */
    public function setMimeType(?string $mimeType): Voice
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return Voice
     */
    public function setFileSize(?int $fileSize): Voice
    {
        $this->fileSize = $fileSize;
        return $this;
    }
}