<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:27
 */

namespace Skipper\Telegram\Entities;

use Skipper\Telegram\ValueObjects\PhotoSize;

class Video
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $width int */
    protected $width;
    /** @var $height int */
    protected $height;
    /** @var $duration int */
    protected $duration;
    /** @var $thumb PhotoSize|null */
    protected $thumb;
    /** @var $mimeType string|null */
    protected $mimeType;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @return null|PhotoSize
     */
    public function getThumb(): ?PhotoSize
    {
        return $this->thumb;
    }

    /**
     * @param null|PhotoSize $thumb
     * @return Video
     */
    public function setThumb(?PhotoSize $thumb): Video
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param null|string $mimeType
     * @return Video
     */
    public function setMimeType(?string $mimeType): Video
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return Video
     */
    public function setFileSize(?int $fileSize): Video
    {
        $this->fileSize = $fileSize;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return Video
     */
    public function setFileId(string $fileId): Video
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Video
     */
    public function setWidth(int $width): Video
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return Video
     */
    public function setHeight(int $height): Video
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return Video
     */
    public function setDuration(int $duration): Video
    {
        $this->duration = $duration;
        return $this;
    }
}