<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:01
 */

namespace Skipper\Telegram\Entities;

class Audio
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $duration int */
    protected $duration;
    /** @var $performer string|null */
    protected $performer;
    /** @var $title string|null */
    protected $title;
    /** @var $mimeType string|null */
    protected $mimeType;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @param string $fileId
     * @return Audio
     */
    public function setFileId(string $fileId): Audio
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param int $duration
     * @return Audio
     */
    public function setDuration(int $duration): Audio
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param null|string $performer
     * @return Audio
     */
    public function setPerformer(?string $performer): Audio
    {
        $this->performer = $performer;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPerformer(): ?string
    {
        return $this->performer;
    }

    /**
     * @param null|string $title
     * @return Audio
     */
    public function setTitle(?string $title): Audio
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $mimeType
     * @return Audio
     */
    public function setMimeType(?string $mimeType): Audio
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param int|null $fileSize
     * @return Audio
     */
    public function setFileSize(?int $fileSize): Audio
    {
        $this->fileSize = $fileSize;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }
}