<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:25
 */

namespace Skipper\Telegram\Entities;

use Skipper\Telegram\ValueObjects\PhotoSize;

class Sticker
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $width int */
    protected $width;
    /** @var $height int */
    protected $height;
    /** @var $thumb PhotoSize|null */
    protected $thumb;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return Sticker
     */
    public function setFileId(string $fileId): Sticker
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Sticker
     */
    public function setWidth(int $width): Sticker
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return Sticker
     */
    public function setHeight(int $height): Sticker
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return null|PhotoSize
     */
    public function getThumb(): ?PhotoSize
    {
        return $this->thumb;
    }

    /**
     * @param null|PhotoSize $thumb
     * @return Sticker
     */
    public function setThumb(?PhotoSize $thumb): Sticker
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return Sticker
     */
    public function setFileSize(?int $fileSize): Sticker
    {
        $this->fileSize = $fileSize;
        return $this;
    }
}