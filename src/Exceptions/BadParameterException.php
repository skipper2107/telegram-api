<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:52
 */

namespace Skipper\Telegram\Exceptions;

use Skipper\Exceptions\Error;

class BadParameterException extends ApiException
{
    protected function addInstantError(): ?Error
    {
        reset($this->data);
        $location = key($this->data);

        return new Error('Invalid parameter', 'invalidParameter', $location ?? 'telegram');
    }
}