<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:51
 */

namespace Skipper\Telegram\Exceptions;

use Skipper\Exceptions\DomainException;
use Skipper\Exceptions\Error;

class ApiException extends DomainException
{
    protected function addInstantError(): ?Error
    {
        return new Error('Telegram API error', 'apiError', 'telegram');
    }
}