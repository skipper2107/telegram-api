<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 2:59 PM
 */

namespace Skipper\Telegram;

use Curl\Curl;
use Skipper\Telegram\Exceptions\ApiException;
use Skipper\Telegram\Factories\ApiRequestTransformer;
use Skipper\Telegram\Sendable\Attachment;
use Skipper\Telegram\Sendable\AudioMessage;
use Skipper\Telegram\Sendable\CallbackQueryAnswer;
use Skipper\Telegram\Sendable\ChatActionMessage;
use Skipper\Telegram\Sendable\ContactMessage;
use Skipper\Telegram\Sendable\DocumentMessage;
use Skipper\Telegram\Sendable\EditMessageCaption;
use Skipper\Telegram\Sendable\EditMessageReplyMarkUp;
use Skipper\Telegram\Sendable\EditMessageText;
use Skipper\Telegram\Sendable\ForwardMessage;
use Skipper\Telegram\Sendable\InlineResponse;
use Skipper\Telegram\Sendable\LocationMessage;
use Skipper\Telegram\Sendable\ManageUserInChatAction;
use Skipper\Telegram\Sendable\Message;
use Skipper\Telegram\Sendable\PhotoMessage;
use Skipper\Telegram\Sendable\StickerMessage;
use Skipper\Telegram\Sendable\VenueMessage;
use Skipper\Telegram\Sendable\VideoMessage;
use Skipper\Telegram\Sendable\VoiceMessage;

class Client
{
    public const TELEGRAM_API_ENDPOINT = 'https://api.telegram.org';
    protected const GET = 'get';
    protected const POST = 'post';

    /**
     * @var string $apiKey
     */
    protected $apiKey;

    /**
     * @var Curl $curl
     */
    protected $curl;

    /**
     * @var ResponseHandler $responseHandler
     */
    protected $responseHandler;

    /**
     * @var $requestTransformer
     */
    protected $requestTransformer;

    /**
     * Client constructor.
     * @param string $apiKey
     * @param Curl|null $curl
     * @param null|ResponseHandler $responseHandler
     * @param null|ApiRequestTransformer $requestTransformer
     * @throws \ErrorException
     */
    public function __construct(
        string $apiKey,
        ?Curl $curl = null,
        ?ResponseHandler $responseHandler = null,
        ?ApiRequestTransformer $requestTransformer = null
    ) {
        $this->apiKey = $apiKey;
        $this->curl = $curl;
        if ($this->curl === null) {
            $this->curl = new Curl(self::TELEGRAM_API_ENDPOINT);
            $curl->setJsonDecoder(function ($response) {
                return $response;
            });
        }
        $this->responseHandler = $responseHandler;
        if ($this->responseHandler === null) {
            $this->responseHandler = new ResponseHandler();
        }
        $this->requestTransformer = $requestTransformer;
        if ($this->requestTransformer === null) {
            $this->requestTransformer = new ApiRequestTransformer();
        }
    }

    /**
     * @return array
     * @throws ApiException
     */
    public function getMe()
    {
        return $this->request(self::GET, '/getMe');
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $data
     * @return array|string|bool
     * @throws ApiException
     */
    private function request(string $method, string $uri, array $data = [])
    {
        $uri = sprintf('/bot%s/%s', $this->apiKey, ltrim($uri, '/'));
        $this->curl->{$method}($uri, $data);
        if ($this->curl->error) {
            throw $this->responseHandler->handleErrors($this->curl);
        }
        return $this->responseHandler->handleResponse($this->curl);
    }

    /**
     * @param Message $message
     * @return array
     * @throws ApiException
     */
    public function sendMessage(Message $message)
    {
        return $this->request(self::POST, '/sendMessage', $this->requestTransformer->createFromTextMessage($message));
    }

    /**
     * @param ForwardMessage $message
     * @return array
     * @throws ApiException
     */
    public function forwardMessage(ForwardMessage $message)
    {
        return $this->request(self::POST, '/forwardMessage', $this->requestTransformer->createFromForward($message));
    }

    /**
     * @param PhotoMessage $photo
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendPhoto(PhotoMessage $photo)
    {
        return $this->sendWithFile('/sendPhoto', $this->requestTransformer->createFromPhoto($photo));
    }

    /**
     * @param string $uri
     * @param array $data
     * @return array
     * @throws ApiException
     */
    protected function sendWithFile(string $uri, array $data)
    {
        foreach ($data as $key => $datum) {
            if ($datum instanceof Attachment) {
                if (null !== $datum->getFileLocation()) {
                    $fileLocation = $datum->getFileLocation();
                    $data[$key] = curl_file_create($fileLocation, mime_content_type($fileLocation), $key);
                    $this->curl->setHeader('Content-Type', 'multipart/form-data');
                } else {
                    $data[$key] = $datum->getFileId();
                }
            }
        }
        return $this->request(self::POST, $uri, $data);
    }

    /**
     * @param DocumentMessage $document
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendDocument(DocumentMessage $document)
    {
        return $this->sendWithFile('/sendDocument', $this->requestTransformer->createFromDocument($document));
    }

    /**
     * @param AudioMessage $audio
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendAudio(AudioMessage $audio)
    {
        return $this->sendWithFile('/sendAudio', $this->requestTransformer->createFromAudio($audio));
    }

    /**
     * @param StickerMessage $sticker
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSticker(StickerMessage $sticker)
    {
        return $this->sendWithFile('/sendSticker', $this->requestTransformer->createFromSticker($sticker));
    }

    /**
     * @param VideoMessage $video
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendVideo(VideoMessage $video)
    {
        return $this->sendWithFile('/sendVideo', $this->requestTransformer->createFromVideo($video));
    }

    /**
     * @param VoiceMessage $voice
     * @return array
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendVoice(VoiceMessage $voice)
    {
        return $this->sendWithFile('/sendVoice', $this->requestTransformer->createFromVoice($voice));
    }

    /**
     * @param LocationMessage $location
     * @return array
     * @throws ApiException
     */
    public function sendLocation(LocationMessage $location)
    {
        return $this->sendWithFile('/sendLocation', $this->requestTransformer->createFromLocationMessage($location));
    }

    /**
     * @param VenueMessage $venue
     * @return array
     * @throws ApiException
     */
    public function sendVenue(VenueMessage $venue)
    {
        return $this->sendWithFile('/sendVenue', $this->requestTransformer->createFromVenueMessage($venue));
    }

    /**
     * @param ContactMessage $contact
     * @return array
     * @throws ApiException
     */
    public function sendContact(ContactMessage $contact)
    {
        return $this->sendWithFile('/sendContact', $this->requestTransformer->createFromContactMessage($contact));
    }

    /**
     * @param ChatActionMessage $action
     * @return array
     * @throws ApiException
     */
    public function sendChatAction(ChatActionMessage $action)
    {
        return $this->sendWithFile('/sendChatAction', $this->requestTransformer->createFromChatAction($action));
    }

    /**
     * @param int $userId
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws ApiException
     */
    public function getUserProfilePhotos(int $userId, ?int $offset = null, ?int $limit = null): array
    {
        return $this->request(self::GET, '/getUserProfilePhotos', [
            'user_id' => $userId,
            'limit' => $limit,
            'offset' => $offset,
        ]);
    }

    /**
     * @param string $locationPath
     * @param string $savePath
     * @return string
     * @throws ApiException
     */
    public function downloadFile(string $locationPath, string $savePath): string
    {
        $this->curl->download(sprintf('/bot%s/%s', $this->apiKey, $locationPath), $savePath);

        if ($this->curl->error) {
            throw $this->responseHandler->handleErrors($this->curl);
        }

        return $savePath;
    }

    /**
     * @param string $fileId
     * @return string
     * @throws ApiException
     */
    public function getFile(string $fileId): string
    {
        $this->responseHandler->setStrategy(ResponseHandler::STRATEGY_STRING);

        return $this->request(self::GET, '/getFile', [
            'file_id' => $fileId,
        ]);
    }

    /**
     * @param ManageUserInChatAction $action
     * @return bool
     * @throws ApiException
     */
    public function kickAndBanFromChat(ManageUserInChatAction $action): bool
    {
        $this->responseHandler->setStrategy(ResponseHandler::STRATEGY_BOOL);

        return $this->request(self::POST, '/kickChatMember',
            $this->requestTransformer->createFromUserChatAction($action));
    }

    /**
     * @param ManageUserInChatAction $action
     * @return bool
     * @throws ApiException
     */
    public function unbanUserInChat(ManageUserInChatAction $action): bool
    {
        $this->responseHandler->setStrategy(ResponseHandler::STRATEGY_BOOL);

        return $this->request(self::POST, '/unbanChatMember',
            $this->requestTransformer->createFromUserChatAction($action));
    }

    /**
     * @param CallbackQueryAnswer $answer
     * @return bool
     * @throws ApiException
     */
    public function answerToCallback(CallbackQueryAnswer $answer): bool
    {
        $this->responseHandler->setStrategy(ResponseHandler::STRATEGY_BOOL);

        return $this->request(self::POST, '/answerCallbackQuery',
            $this->requestTransformer->createFromCallbackAnswer($answer));
    }

    /**
     * @param InlineResponse $response
     * @return bool
     * @throws ApiException
     */
    public function sendInlineResult(InlineResponse $response): bool
    {
        $this->responseHandler->setStrategy(ResponseHandler::STRATEGY_BOOL);

        return $this->request(self::POST, '/answerInlineQuery',
            $this->requestTransformer->createFromInlineResponse($response));
    }

    /**
     * @param EditMessageCaption $message
     * @return array
     * @throws ApiException
     */
    public function editMessageCaption(EditMessageCaption $message): array
    {
        return $this->request(self::POST, '/editMessageCaption',
            $this->requestTransformer->createFromEditCaption($message));
    }

    /**
     * @param EditMessageText $message
     * @return array
     * @throws ApiException
     */
    public function editMessageText(EditMessageText $message): array
    {
        return $this->request(self::POST, '/editMessageText', $this->requestTransformer->createFromEditText($message));
    }

    /**
     * @param EditMessageReplyMarkUp $message
     * @return array
     * @throws ApiException
     */
    public function editMessageReplyMarkUp(EditMessageReplyMarkUp $message): array
    {
        return $this->request(self::POST, '/editMessageReplyMarkup',
            $this->requestTransformer->createFromEditReplyMarkUp($message));
    }
}