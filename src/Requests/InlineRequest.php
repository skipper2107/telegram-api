<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 3:27 PM
 */

namespace Skipper\Telegram\Requests;

use Skipper\Telegram\Entities\User;
use Skipper\Telegram\ValueObjects\Location;

class InlineRequest
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var User
     */
    protected $from;

    /**
     * @var Location|null
     */
    protected $location;

    /**
     * @var string
     */
    protected $query;

    /**
     * @var string
     */
    protected $offset;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return InlineRequest
     */
    public function setId(string $id): InlineRequest
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getFrom(): User
    {
        return $this->from;
    }

    /**
     * @param User $from
     * @return InlineRequest
     */
    public function setFrom(User $from): InlineRequest
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return null|Location
     */
    public function getLocation(): ?Location
    {
        return $this->location;
    }

    /**
     * @param null|Location $location
     * @return InlineRequest
     */
    public function setLocation(?Location $location): InlineRequest
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     * @return InlineRequest
     */
    public function setQuery(string $query): InlineRequest
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return $this->offset;
    }

    /**
     * @param string $offset
     * @return InlineRequest
     */
    public function setOffset(string $offset): InlineRequest
    {
        $this->offset = $offset;
        return $this;
    }
}