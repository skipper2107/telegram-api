<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/1/18
 * Time: 3:01 PM
 */

namespace Skipper\Telegram;

use Curl\Curl;
use Skipper\Telegram\Exceptions\ApiException;
use Skipper\Telegram\Exceptions\BadParameterException;

class ResponseHandler
{

    public const STRATEGY_JSON = 'json';
    public const STRATEGY_STRING = 'string';
    public const STRATEGY_BOOL = 'bool';

    /**
     * @var string
     */
    protected $strategy = self::STRATEGY_JSON;

    /**
     * @param Curl $curl
     * @return ApiException
     */
    public function handleErrors(Curl $curl): ApiException
    {
        return new ApiException($curl->errorMessage, [
            'url' => $curl->url,
        ], null, $curl->httpStatusCode);
    }

    /**
     * @param Curl $curl
     * @return array|string|bool
     * @throws BadParameterException
     */
    public function handleResponse(Curl $curl)
    {
        switch ($this->strategy) {
            case static::STRATEGY_JSON:
                return json_decode($curl->response, true);
            case static::STRATEGY_STRING:
                return $curl->response;
            case static::STRATEGY_BOOL:
                switch ($curl->response) {
                    case 'true':
                        return true;
                    case 'false':
                        return false;
                    default:
                        return (bool)$curl->response;
                }
            default:
                throw new BadParameterException('UnknownResponseHandlerStrategy:' . $this->strategy, [
                    'strategy' => $this->strategy,
                ]);
        }
    }

    /**
     * @param string $strategy
     * @return ResponseHandler
     */
    public function setStrategy(string $strategy): ResponseHandler
    {
        $this->strategy = $strategy;

        return $this;
    }
}