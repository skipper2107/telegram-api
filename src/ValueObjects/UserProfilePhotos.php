<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 16:24
 */

namespace Skipper\Telegram\ValueObjects;

class UserProfilePhotos
{
    /** @var $totalCount int */
    protected $totalCount;
    /** @var $photos PhotoSize[] */
    protected $photos;

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return UserProfilePhotos
     */
    public function setTotalCount(int $totalCount): UserProfilePhotos
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    /**
     * @return PhotoSize[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param PhotoSize[] $photos
     * @return UserProfilePhotos
     */
    public function setPhotos(array $photos): UserProfilePhotos
    {
        $this->photos = $photos;
        return $this;
    }
}