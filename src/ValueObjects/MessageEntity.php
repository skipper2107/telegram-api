<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 14:43
 */

namespace Skipper\Telegram\ValueObjects;

use Skipper\Telegram\Enumerators\MessageEntityEnumerator;

class MessageEntity
{
    /** @var $type string */
    protected $type;
    /** @var $offset int */
    protected $offset;
    /** @var $length int */
    protected $length;
    /** @var $url string|null */
    protected $url;

    /**
     * MessageEntity constructor.
     * @param string $type
     * @throws \ReflectionException
     */
    public function __construct(string $type)
    {
        $this->type = (new MessageEntityEnumerator)->normalize($type);
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @param int $length
     * @return MessageEntity
     */
    public function setLength(int $length): MessageEntity
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return MessageEntity
     */
    public function setOffset(int $offset): MessageEntity
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return MessageEntity
     */
    public function setUrl(?string $url): MessageEntity
    {
        $this->url = $url;
        return $this;
    }
}