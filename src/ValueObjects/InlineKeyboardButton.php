<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/3/18
 * Time: 10:26 AM
 */

namespace Skipper\Telegram\ValueObjects;

class InlineKeyboardButton
{
    /** @var $text string */
    protected $text;
    /** @var $url string|null */
    protected $url;
    /** @var $callbackData string|null */
    protected $callbackData;
    /** @var $switchInlineQuery string|null */
    protected $switchInlineQuery;
    /** @var $switchInlineQueryCurrentChat string|null */
    protected $switchInlineQueryCurrentChat;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return InlineKeyboardButton
     */
    public function setText(string $text): InlineKeyboardButton
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return InlineKeyboardButton
     */
    public function setUrl(?string $url): InlineKeyboardButton
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCallbackData(): ?string
    {
        return $this->callbackData;
    }

    /**
     * @param null|string $callbackData
     * @return InlineKeyboardButton
     */
    public function setCallbackData(?string $callbackData): InlineKeyboardButton
    {
        $this->callbackData = $callbackData;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSwitchInlineQuery(): ?string
    {
        return $this->switchInlineQuery;
    }

    /**
     * @param null|string $switchInlineQuery
     * @return InlineKeyboardButton
     */
    public function setSwitchInlineQuery(?string $switchInlineQuery): InlineKeyboardButton
    {
        $this->switchInlineQuery = $switchInlineQuery;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSwitchInlineQueryCurrentChat(): ?string
    {
        return $this->switchInlineQueryCurrentChat;
    }

    /**
     * @param null|string $switchInlineQueryCurrentChat
     * @return InlineKeyboardButton
     */
    public function setSwitchInlineQueryCurrentChat(?string $switchInlineQueryCurrentChat): InlineKeyboardButton
    {
        $this->switchInlineQueryCurrentChat = $switchInlineQueryCurrentChat;
        return $this;
    }
}