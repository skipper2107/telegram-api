<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/3/18
 * Time: 10:13 AM
 */

namespace Skipper\Telegram\ValueObjects;

class KeyboardButton
{
    /** @var $text string */
    protected $text;
    /** @var $requestContact bool|null */
    protected $requestContact;
    /** @var $requestLocation bool|null */
    protected $requestLocation;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return KeyboardButton
     */
    public function setText(string $text): KeyboardButton
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequestContact(): ?bool
    {
        return $this->requestContact;
    }

    /**
     * @param bool|null $requestContact
     * @return KeyboardButton
     */
    public function setRequestContact(?bool $requestContact): KeyboardButton
    {
        $this->requestContact = $requestContact;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequestLocation(): ?bool
    {
        return $this->requestLocation;
    }

    /**
     * @param bool|null $requestLocation
     * @return KeyboardButton
     */
    public function setRequestLocation(?bool $requestLocation): KeyboardButton
    {
        $this->requestLocation = $requestLocation;
        return $this;
    }
}