<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/12/18
 * Time: 3:27 PM
 */

namespace Skipper\Telegram\ValueObjects;

use Skipper\Telegram\Entities\Message;
use Skipper\Telegram\Entities\User;

class CallbackQuery
{
    /** @var $id string */
    protected $id;
    /** @var $from User */
    protected $from;
    /** @var $message Message|null */
    protected $message;
    /** @var $inlineMessageId string|null */
    protected $inlineMessageId;
    /** @var $data string */
    protected $data;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CallbackQuery
     */
    public function setId(string $id): CallbackQuery
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getFrom(): User
    {
        return $this->from;
    }

    /**
     * @param User $from
     * @return CallbackQuery
     */
    public function setFrom(User $from): CallbackQuery
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return null|Message
     */
    public function getMessage(): ?Message
    {
        return $this->message;
    }

    /**
     * @param null|Message $message
     * @return CallbackQuery
     */
    public function setMessage(?Message $message): CallbackQuery
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getInlineMessageId(): ?string
    {
        return $this->inlineMessageId;
    }

    /**
     * @param null|string $inlineMessageId
     * @return CallbackQuery
     */
    public function setInlineMessageId(?string $inlineMessageId): CallbackQuery
    {
        $this->inlineMessageId = $inlineMessageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return CallbackQuery
     */
    public function setData(string $data): CallbackQuery
    {
        $this->data = $data;
        return $this;
    }
}