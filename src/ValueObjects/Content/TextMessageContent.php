<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:04 PM
 */

namespace Skipper\Telegram\ValueObjects\Inline\Content;

use Skipper\Telegram\Contracts\TextContract;
use Skipper\Telegram\Traits\MessageTrait;

class TextMessageContent implements ContentInterface, TextContract
{
    use MessageTrait;
}