<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:06 PM
 */

namespace Skipper\Telegram\ValueObjects\Inline\Content;

use Skipper\Telegram\Contracts\LocationContract;
use Skipper\Telegram\Traits\HasLocation;

class LocationContent implements ContentInterface, LocationContract
{
    use HasLocation;
}