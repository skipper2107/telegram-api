<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:18 PM
 */

namespace Skipper\Telegram\ValueObjects\Inline\Content;

use Skipper\Telegram\Contracts\VenueContract;
use Skipper\Telegram\Traits\HasVenue;

class VenueContent extends LocationContent implements VenueContract
{
    use HasVenue;
}