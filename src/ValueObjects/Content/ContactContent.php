<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/17/18
 * Time: 10:33 AM
 */

namespace Skipper\Telegram\ValueObjects\Inline\Content;

use Skipper\Telegram\Contracts\ContactContract;
use Skipper\Telegram\Traits\HasContact;

class ContactContent implements ContentInterface, ContactContract
{
    use HasContact;
}