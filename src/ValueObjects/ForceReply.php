<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/12/18
 * Time: 5:44 PM
 */

namespace Skipper\Telegram\ValueObjects;

class ForceReply
{
    /** @var $forceReply bool */
    protected $forceReply;
    /** @var $selective bool|null */
    protected $selective;

    /**
     * @return bool
     */
    public function isForceReply(): bool
    {
        return $this->forceReply;
    }

    /**
     * @param bool $forceReply
     * @return ForceReply
     */
    public function setForceReply(bool $forceReply): ForceReply
    {
        $this->forceReply = $forceReply;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSelective(): ?bool
    {
        return $this->selective;
    }

    /**
     * @param bool|null $selective
     * @return ForceReply
     */
    public function setSelective(?bool $selective): ForceReply
    {
        $this->selective = $selective;
        return $this;
    }
}