<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:52
 */

namespace Skipper\Telegram\ValueObjects;

class Venue
{
    /** @var $location Location */
    protected $location;
    /** @var $title string */
    protected $title;
    /** @var $address string */
    protected $address;

    /**
     * @return Location
     */
    public function getLocation(): Location
    {
        return $this->location;
    }

    /**
     * @param Location $location
     * @return Venue
     */
    public function setLocation(Location $location): Venue
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Venue
     */
    public function setTitle(string $title): Venue
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Venue
     */
    public function setAddress(string $address): Venue
    {
        $this->address = $address;
        return $this;
    }
}