<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/3/18
 * Time: 10:23 AM
 */

namespace Skipper\Telegram\ValueObjects;

class InlineKeyboardMarkup
{
    /** @var $inlineKeyboard InlineKeyboardButton[][] */
    protected $inlineKeyboard;

    /**
     * @return InlineKeyboardButton[][]
     */
    public function getInlineKeyboard(): array
    {
        return $this->inlineKeyboard;
    }

    /**
     * @param InlineKeyboardButton[][] $inlineKeyboard
     * @return InlineKeyboardMarkup
     */
    public function setInlineKeyboard(array $inlineKeyboard): InlineKeyboardMarkup
    {
        $this->inlineKeyboard = $inlineKeyboard;
        return $this;
    }
}