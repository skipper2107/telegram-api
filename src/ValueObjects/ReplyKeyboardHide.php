<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/3/18
 * Time: 10:21 AM
 */

namespace Skipper\Telegram\ValueObjects;

class ReplyKeyboardHide
{
    /** @var $hideKeyboard bool */
    protected $hideKeyboard;
    /** @var $selective bool|null */
    protected $selective;

    /**
     * @return bool
     */
    public function isHideKeyboard(): bool
    {
        return $this->hideKeyboard;
    }

    /**
     * @param bool $hideKeyboard
     * @return ReplyKeyboardHide
     */
    public function setHideKeyboard(bool $hideKeyboard): ReplyKeyboardHide
    {
        $this->hideKeyboard = $hideKeyboard;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSelective(): ?bool
    {
        return $this->selective;
    }

    /**
     * @param bool|null $selective
     * @return ReplyKeyboardHide
     */
    public function setSelective(?bool $selective): ReplyKeyboardHide
    {
        $this->selective = $selective;
        return $this;
    }
}