<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:21
 */

namespace Skipper\Telegram\ValueObjects;

class PhotoSize
{
    /** @var $fileId string */
    protected $fileId;
    /** @var $width int */
    protected $width;
    /** @var $height int */
    protected $height;
    /** @var $fileSize int|null */
    protected $fileSize;

    /**
     * @return string
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * @param string $fileId
     * @return PhotoSize
     */
    public function setFileId(string $fileId): PhotoSize
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return PhotoSize
     */
    public function setWidth(int $width): PhotoSize
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return PhotoSize
     */
    public function setHeight(int $height): PhotoSize
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return PhotoSize
     */
    public function setFileSize(?int $fileSize): PhotoSize
    {
        $this->fileSize = $fileSize;
        return $this;
    }
}