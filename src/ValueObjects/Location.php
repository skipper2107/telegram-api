<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 02.03.18
 * Time: 15:50
 */

namespace Skipper\Telegram\ValueObjects;

use Skipper\Telegram\Traits\HasLocation;

class Location
{
    use HasLocation;
}