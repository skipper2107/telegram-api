<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/17/18
 * Time: 4:02 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Traits\HasAttachment;

class PhotoMessage extends SimpleMessage
{
    use HasAttachment;
}