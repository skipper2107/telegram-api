<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:18 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Contracts\TextContract;
use Skipper\Telegram\Traits\MessageTrait;

class EditMessageText extends SimpleEditMessage implements TextContract
{
    use MessageTrait;
}