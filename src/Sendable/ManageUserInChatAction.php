<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/5/18
 * Time: 4:41 PM
 */

namespace Skipper\Telegram\Sendable;

class ManageUserInChatAction extends AbstractMessage
{
    /**
     * @var int
     */
    protected $userId;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return ManageUserInChatAction
     */
    public function setUserId(int $userId): ManageUserInChatAction
    {
        $this->userId = $userId;
        return $this;
    }
}