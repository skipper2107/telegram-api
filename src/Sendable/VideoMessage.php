<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 11:55 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Traits\HasAttachment;

class VideoMessage extends SimpleMessage
{
    use HasAttachment;

    /** @var $duration int|null */
    protected $duration;
    /** @var $width int|null */
    protected $width;
    /** @var $height int|null */
    protected $height;

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return VideoMessage
     */
    public function setDuration(?int $duration): VideoMessage
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     * @return VideoMessage
     */
    public function setWidth(?int $width): VideoMessage
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     * @return VideoMessage
     */
    public function setHeight(?int $height): VideoMessage
    {
        $this->height = $height;
        return $this;
    }
}