<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/5/18
 * Time: 4:37 PM
 */

namespace Skipper\Telegram\Sendable;

class CallbackQueryAnswer
{
    /**
     * @var string
     */
    protected $callbackQueryId;

    /**
     * @var string|null
     */
    protected $text;

    /**
     * @var bool|null
     */
    protected $showAlert = false;

    /**
     * @var string|null
     */
    protected $url;

    /**
     * @return string
     */
    public function getCallbackQueryId(): string
    {
        return $this->callbackQueryId;
    }

    /**
     * @param string $callbackQueryId
     * @return CallbackQueryAnswer
     */
    public function setCallbackQueryId(string $callbackQueryId): CallbackQueryAnswer
    {
        $this->callbackQueryId = $callbackQueryId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param null|string $text
     * @return CallbackQueryAnswer
     */
    public function setText(?string $text): CallbackQueryAnswer
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getShowAlert(): ?bool
    {
        return $this->showAlert;
    }

    /**
     * @param bool|null $showAlert
     * @return CallbackQueryAnswer
     */
    public function setShowAlert(?bool $showAlert): CallbackQueryAnswer
    {
        $this->showAlert = $showAlert;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return CallbackQueryAnswer
     */
    public function setUrl(?string $url): CallbackQueryAnswer
    {
        $this->url = $url;
        return $this;
    }
}