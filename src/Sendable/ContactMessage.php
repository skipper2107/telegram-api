<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 12:02 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Contracts\ContactContract;
use Skipper\Telegram\Traits\HasContact;

class ContactMessage extends SimpleMessage implements ContactContract
{
    use HasContact;
}