<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/17/18
 * Time: 4:19 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Traits\HasAttachment;

class AudioMessage extends SimpleMessage
{
    use HasAttachment;

    /** @var $duration int|null */
    protected $duration;
    /** @var $performer string|null */
    protected $performer;
    /** @var $title string|null */
    protected $title;

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return AudioMessage
     */
    public function setDuration(?int $duration): AudioMessage
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPerformer(): ?string
    {
        return $this->performer;
    }

    /**
     * @param null|string $performer
     * @return AudioMessage
     */
    public function setPerformer(?string $performer): AudioMessage
    {
        $this->performer = $performer;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return AudioMessage
     */
    public function setTitle(?string $title): AudioMessage
    {
        $this->title = $title;
        return $this;
    }

}