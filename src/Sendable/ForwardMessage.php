<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/17/18
 * Time: 3:38 PM
 */

namespace Skipper\Telegram\Sendable;

class ForwardMessage extends AbstractMessage
{
    /** @var $fromChatId string|int */
    protected $fromChatId;
    /** @var $disableNotification bool|null */
    protected $disableNotification;
    /** @var $messageId int */
    protected $messageId;

    /**
     * @return int|string
     */
    public function getFromChatId()
    {
        return $this->fromChatId;
    }

    /**
     * @param int|string $fromChatId
     * @return ForwardMessage
     */
    public function setFromChatId($fromChatId)
    {
        $this->fromChatId = $fromChatId;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDisableNotification(): ?bool
    {
        return $this->disableNotification;
    }

    /**
     * @param bool|null $disableNotification
     * @return ForwardMessage
     */
    public function setDisableNotification(?bool $disableNotification): ForwardMessage
    {
        $this->disableNotification = $disableNotification;
        return $this;
    }

    /**
     * @return int
     */
    public function getMessageId(): int
    {
        return $this->messageId;
    }

    /**
     * @param int $messageId
     * @return ForwardMessage
     */
    public function setMessageId(int $messageId): ForwardMessage
    {
        $this->messageId = $messageId;
        return $this;
    }
}