<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 12:25 PM
 */

namespace Skipper\Telegram\Sendable;

abstract class AbstractMessage
{
    /**
     * @var string|int
     */
    protected $chatId;

    /**
     * @return int|string
     */
    public function getChatId()
    {
        return $this->chatId;
    }

    /**
     * @param int|string $chatId
     * @return AbstractMessage
     */
    public function setChatId($chatId): AbstractMessage
    {
        $this->chatId = $chatId;
        return $this;
    }
}