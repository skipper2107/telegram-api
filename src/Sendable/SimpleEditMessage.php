<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:06 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\ValueObjects\InlineKeyboardMarkup;

abstract class SimpleEditMessage extends AbstractMessage
{
    /**
     * @var int|null
     */
    protected $messageId;

    /**
     * @var string|null
     */
    protected $inlineMessageId;

    /**
     * @var InlineKeyboardMarkup|null
     */
    protected $replyMarkUp;

    /**
     * @return int|null
     */
    public function getMessageId(): ?int
    {
        return $this->messageId;
    }

    /**
     * @param int|null $messageId
     * @return SimpleEditMessage
     */
    public function setMessageId(?int $messageId): SimpleEditMessage
    {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getInlineMessageId(): ?string
    {
        return $this->inlineMessageId;
    }

    /**
     * @param null|string $inlineMessageId
     * @return SimpleEditMessage
     */
    public function setInlineMessageId(?string $inlineMessageId): SimpleEditMessage
    {
        $this->inlineMessageId = $inlineMessageId;
        return $this;
    }

    /**
     * @return null|InlineKeyboardMarkup
     */
    public function getReplyMarkUp(): ?InlineKeyboardMarkup
    {
        return $this->replyMarkUp;
    }

    /**
     * @param null|InlineKeyboardMarkup $replyMarkUp
     * @return SimpleEditMessage
     */
    public function setReplyMarkUp(?InlineKeyboardMarkup $replyMarkUp): SimpleEditMessage
    {
        $this->replyMarkUp = $replyMarkUp;
        return $this;
    }

}