<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:24 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultMpeg4Gif extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::MPEG4_GIF;

    /**
     * @var string
     */
    protected $thumbUrl;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @return string
     */
    public function getThumbUrl(): string
    {
        return $this->thumbUrl;
    }

    /**
     * @param string $thumbUrl
     * @return InlineQueryResultMpeg4Gif
     */
    public function setThumbUrl(string $thumbUrl): InlineQueryResultMpeg4Gif
    {
        $this->thumbUrl = $thumbUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultMpeg4Gif
     */
    public function setCaption(?string $caption): InlineQueryResultMpeg4Gif
    {
        $this->caption = $caption;
        return $this;
    }
}