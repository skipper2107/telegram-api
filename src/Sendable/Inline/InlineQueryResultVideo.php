<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:26 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Enumerators\MimeTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultVideo extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::VIDEO;

    /**
     * @var string
     */
    protected $mime;

    /**
     * @var string
     */
    protected $thumbUrl;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @var int|null
     */
    protected $duration;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     * @return InlineQueryResultVideo
     * @throws \ReflectionException
     */
    public function setMime(string $mime): InlineQueryResultVideo
    {
        $this->mime = (new MimeTypeEnumerator)->normalize($mime);
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbUrl(): string
    {
        return $this->thumbUrl;
    }

    /**
     * @param string $thumbUrl
     * @return InlineQueryResultVideo
     */
    public function setThumbUrl(string $thumbUrl): InlineQueryResultVideo
    {
        $this->thumbUrl = $thumbUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultVideo
     */
    public function setCaption(?string $caption): InlineQueryResultVideo
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return InlineQueryResultVideo
     */
    public function setDuration(?int $duration): InlineQueryResultVideo
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return InlineQueryResultVideo
     */
    public function setDescription(?string $description): InlineQueryResultVideo
    {
        $this->description = $description;
        return $this;
    }
}