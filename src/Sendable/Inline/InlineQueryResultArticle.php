<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 12:54 PM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;
use Skipper\Telegram\ValueObjects\Inline\Content\ContentInterface;
use Skipper\Telegram\ValueObjects\InlineKeyboardMarkup;

class InlineQueryResultArticle extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::ARTICLE;

    /**
     * @var InlineKeyboardMarkup|null
     */
    protected $replyMarkUp;

    /**
     * @var string|null
     */
    protected $url;

    /**
     * @var bool|null
     */
    protected $hideUrl;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * InlineQueryResultArticle constructor.
     * @param string $title
     * @param ContentInterface $content
     * @throws \ReflectionException
     */
    public function __construct(string $title, ContentInterface $content)
    {
        parent::__construct();
        $this->setTitle($title)
            ->setContent($content);
    }

    /**
     * @return ContentInterface
     */
    public function getContent(): ContentInterface
    {
        return $this->content;
    }

    /**
     * @return null|InlineKeyboardMarkup
     */
    public function getReplyMarkUp(): ?InlineKeyboardMarkup
    {
        return $this->replyMarkUp;
    }

    /**
     * @param null|InlineKeyboardMarkup $replyMarkUp
     * @return InlineQueryResultArticle
     */
    public function setReplyMarkUp(?InlineKeyboardMarkup $replyMarkUp): BaseInlineResult
    {
        $this->replyMarkUp = $replyMarkUp;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return InlineQueryResultArticle
     */
    public function setUrl(?string $url): InlineQueryResultArticle
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHideUrl(): ?bool
    {
        return $this->hideUrl;
    }

    /**
     * @param bool|null $hideUrl
     * @return InlineQueryResultArticle
     */
    public function setHideUrl(?bool $hideUrl): InlineQueryResultArticle
    {
        $this->hideUrl = $hideUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return InlineQueryResultArticle
     */
    public function setDescription(?string $description): InlineQueryResultArticle
    {
        $this->description = $description;
        return $this;
    }
}