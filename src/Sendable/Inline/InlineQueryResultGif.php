<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 2:05 PM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultGif extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::GIF;

    /**
     * @var string
     */
    protected $thumbUrl;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @return string
     */
    public function getThumbUrl(): string
    {
        return $this->thumbUrl;
    }

    /**
     * @param string $thumbUrl
     * @return InlineQueryResultGif
     */
    public function setThumbUrl(string $thumbUrl): InlineQueryResultGif
    {
        $this->thumbUrl = $thumbUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultGif
     */
    public function setCaption(?string $caption): InlineQueryResultGif
    {
        $this->caption = $caption;
        return $this;
    }
}