<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:33 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\ValueObjects\Inline\Content\ContentInterface;
use Skipper\Telegram\ValueObjects\InlineKeyboardMarkup;

abstract class BaseInlineResult
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string|null
     */
    protected $title;

    /**
     * @var ContentInterface|null
     */
    protected $content;

    /**
     * @var InlineKeyboardMarkup|null
     */
    protected $replyMarkUp;

    /**
     * BaseInlineResult constructor.
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->id = bin2hex(openssl_random_pseudo_bytes(64));
        $this->type = (new InlineQueryResultTypeEnumerator)->normalize($this->type);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BaseInlineResult
     */
    public function setId(string $id): BaseInlineResult
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return BaseInlineResult
     */
    public function setTitle(?string $title): BaseInlineResult
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return null|ContentInterface
     */
    public function getContent(): ?ContentInterface
    {
        return $this->content;
    }

    /**
     * @param null|ContentInterface $content
     * @return BaseInlineResult
     */
    public function setContent(?ContentInterface $content): BaseInlineResult
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return null|InlineKeyboardMarkup
     */
    public function getReplyMarkUp(): ?InlineKeyboardMarkup
    {
        return $this->replyMarkUp;
    }

    /**
     * @param null|InlineKeyboardMarkup $replyMarkUp
     * @return BaseInlineResult
     */
    public function setReplyMarkUp(?InlineKeyboardMarkup $replyMarkUp): BaseInlineResult
    {
        $this->replyMarkUp = $replyMarkUp;
        return $this;
    }
}