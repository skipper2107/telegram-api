<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 11:10 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\VenueContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasVenue;

class InlineQueryResultVenue extends InlineQueryResultLocation implements VenueContract
{
    use HasVenue;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::VENUE;
}