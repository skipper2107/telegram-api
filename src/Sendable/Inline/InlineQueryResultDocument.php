<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:46 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Enumerators\MimeTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultDocument extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::DOCUMENT;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @var string
     */
    protected $documentUrl;

    /**
     * @var string
     */
    protected $mime;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * InlineQueryResultDocument constructor.
     * @param string $title
     * @throws \ReflectionException
     */
    public function __construct(string $title)
    {
        parent::__construct();
        $this->setTitle($title);
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultDocument
     */
    public function setCaption(?string $caption): InlineQueryResultDocument
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentUrl(): string
    {
        return $this->documentUrl;
    }

    /**
     * @param string $documentUrl
     * @return InlineQueryResultDocument
     */
    public function setDocumentUrl(string $documentUrl): InlineQueryResultDocument
    {
        $this->documentUrl = $documentUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     * @return InlineQueryResultDocument
     * @throws \ReflectionException
     */
    public function setMime(string $mime): InlineQueryResultDocument
    {
        $this->mime = (new MimeTypeEnumerator)->normalize($mime);
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return InlineQueryResultDocument
     */
    public function setDescription(?string $description): InlineQueryResultDocument
    {
        $this->description = $description;
        return $this;
    }
}