<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:33 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;

class InlineQueryResultAudio extends BaseInlineResult
{
    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::AUDIO;

    /**
     * @var string
     */
    protected $audioUrl;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @var string|null
     */
    protected $performer;

    /**
     * @var int|null
     */
    protected $duration;

    /**
     * @return string
     */
    public function getAudioUrl(): string
    {
        return $this->audioUrl;
    }

    /**
     * @param string $audioUrl
     * @return InlineQueryResultAudio
     */
    public function setAudioUrl(string $audioUrl): InlineQueryResultAudio
    {
        $this->audioUrl = $audioUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultAudio
     */
    public function setCaption(?string $caption): InlineQueryResultAudio
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPerformer(): ?string
    {
        return $this->performer;
    }

    /**
     * @param null|string $performer
     * @return InlineQueryResultAudio
     */
    public function setPerformer(?string $performer): InlineQueryResultAudio
    {
        $this->performer = $performer;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return InlineQueryResultAudio
     */
    public function setDuration(?int $duration): InlineQueryResultAudio
    {
        $this->duration = $duration;
        return $this;
    }
}