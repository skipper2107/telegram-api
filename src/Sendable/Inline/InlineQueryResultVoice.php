<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 10:39 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;

class InlineQueryResultVoice extends BaseInlineResult
{
    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::VOICE;

    /**
     * @var string
     */
    protected $voiceUrl;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @var int|null
     */
    protected $duration;

    /**
     * InlineQueryResultVoice constructor.
     * @param string $title
     * @throws \ReflectionException
     */
    public function __construct(string $title)
    {
        parent::__construct();
        $this->setTitle($title);
    }

    /**
     * @return string
     */
    public function getVoiceUrl(): string
    {
        return $this->voiceUrl;
    }

    /**
     * @param string $voiceUrl
     * @return InlineQueryResultVoice
     */
    public function setVoiceUrl(string $voiceUrl): InlineQueryResultVoice
    {
        $this->voiceUrl = $voiceUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultVoice
     */
    public function setCaption(?string $caption): InlineQueryResultVoice
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return InlineQueryResultVoice
     */
    public function setDuration(?int $duration): InlineQueryResultVoice
    {
        $this->duration = $duration;
        return $this;
    }


}