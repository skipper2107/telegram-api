<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/13/18
 * Time: 11:08 AM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\LocationContract;
use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasLocation;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultLocation extends BaseInlineResult implements ThumbContract, LocationContract
{
    use HasLocation;
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::LOCATION;

    /**
     * InlineQueryResultLocation constructor.
     * @param string $title
     * @throws \ReflectionException
     */
    public function __construct(string $title)
    {
        parent::__construct();
        $this->setTitle($title);
    }

}