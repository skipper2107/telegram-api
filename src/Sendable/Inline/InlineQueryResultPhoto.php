<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 1:52 PM
 */

namespace Skipper\Telegram\Sendable\Inline;

use Skipper\Telegram\Contracts\ThumbContract;
use Skipper\Telegram\Enumerators\InlineQueryResultTypeEnumerator;
use Skipper\Telegram\Traits\HasSize;

class InlineQueryResultPhoto extends BaseInlineResult implements ThumbContract
{
    use HasSize;

    /**
     * @var string
     */
    protected $type = InlineQueryResultTypeEnumerator::PHOTO;

    /**
     * @var string
     */
    protected $thumbUrl;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * @var string|null
     */
    protected $caption;

    /**
     * @return string
     */
    public function getThumbUrl(): string
    {
        return $this->thumbUrl;
    }

    /**
     * @param string $thumbUrl
     * @return InlineQueryResultPhoto
     */
    public function setThumbUrl(string $thumbUrl): InlineQueryResultPhoto
    {
        $this->thumbUrl = $thumbUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return InlineQueryResultPhoto
     */
    public function setDescription(?string $description): InlineQueryResultPhoto
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @param null|string $caption
     * @return InlineQueryResultPhoto
     */
    public function setCaption(?string $caption): InlineQueryResultPhoto
    {
        $this->caption = $caption;
        return $this;
    }
}