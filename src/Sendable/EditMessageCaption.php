<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:24 AM
 */

namespace Skipper\Telegram\Sendable;

class EditMessageCaption extends SimpleEditMessage
{
    /**
     * @var string
     */
    protected $caption;

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     * @return EditMessageCaption
     */
    public function setCaption(string $caption): EditMessageCaption
    {
        $this->caption = $caption;
        return $this;
    }
}