<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 11:57 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Contracts\LocationContract;
use Skipper\Telegram\Traits\HasLocation;

class LocationMessage extends SimpleMessage implements LocationContract
{
    use HasLocation;
}