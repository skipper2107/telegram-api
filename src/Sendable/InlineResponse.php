<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 4/6/18
 * Time: 11:31 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Sendable\Inline\BaseInlineResult;

class InlineResponse
{
    /**
     * @var string
     */
    protected $inlineQueryId;

    /**
     * @var BaseInlineResult[]
     */
    protected $results = [];

    /**
     * @var int|null
     */
    protected $cacheTime;

    /**
     * @var bool|null
     */
    protected $isPersonal;

    /**
     * @var string|null
     */
    protected $nextOffset;

    /**
     * @var string|null
     */
    protected $switchPmText;

    /**
     * @var string|null
     */
    protected $switchPmParameter;

    /**
     * @return string
     */
    public function getInlineQueryId(): string
    {
        return $this->inlineQueryId;
    }

    /**
     * @param string $inlineQueryId
     * @return InlineResponse
     */
    public function setInlineQueryId(string $inlineQueryId): InlineResponse
    {
        $this->inlineQueryId = $inlineQueryId;
        return $this;
    }

    /**
     * @return BaseInlineResult[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param BaseInlineResult[] $results
     * @return InlineResponse
     */
    public function setResults(array $results): InlineResponse
    {
        $this->results = $results;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCacheTime(): ?int
    {
        return $this->cacheTime;
    }

    /**
     * @param int|null $cacheTime
     * @return InlineResponse
     */
    public function setCacheTime(?int $cacheTime): InlineResponse
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPersonal(): ?bool
    {
        return $this->isPersonal;
    }

    /**
     * @param bool|null $isPersonal
     * @return InlineResponse
     */
    public function setIsPersonal(?bool $isPersonal): InlineResponse
    {
        $this->isPersonal = $isPersonal;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNextOffset(): ?string
    {
        return $this->nextOffset;
    }

    /**
     * @param null|string $nextOffset
     * @return InlineResponse
     */
    public function setNextOffset(?string $nextOffset): InlineResponse
    {
        $this->nextOffset = $nextOffset;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSwitchPmText(): ?string
    {
        return $this->switchPmText;
    }

    /**
     * @param null|string $switchPmText
     * @return InlineResponse
     */
    public function setSwitchPmText(?string $switchPmText): InlineResponse
    {
        $this->switchPmText = $switchPmText;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSwitchPmParameter(): ?string
    {
        return $this->switchPmParameter;
    }

    /**
     * @param null|string $switchPmParameter
     * @return InlineResponse
     */
    public function setSwitchPmParameter(?string $switchPmParameter): InlineResponse
    {
        $this->switchPmParameter = $switchPmParameter;
        return $this;
    }

    /**
     * @param BaseInlineResult $result
     * @return InlineResponse
     */
    public function addResult(BaseInlineResult $result): InlineResponse
    {
        $this->results[] = $result;

        return $this;
    }


}