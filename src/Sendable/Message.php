<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/12/18
 * Time: 4:16 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Contracts\TextContract;
use Skipper\Telegram\Traits\MessageTrait;

class Message extends SimpleMessage implements TextContract
{
    use MessageTrait;

    /**
     * Message constructor.
     * @param $chatId string|int
     * @param string $text
     */
    public function __construct($chatId, string $text)
    {
        $this->chatId = $chatId;
        $this->text = $text;
    }
}