<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/17/18
 * Time: 4:44 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\ValueObjects\ForceReply;
use Skipper\Telegram\ValueObjects\InlineKeyboardMarkup;
use Skipper\Telegram\ValueObjects\ReplyKeyboardHide;
use Skipper\Telegram\ValueObjects\ReplyKeyboardMarkup;

abstract class SimpleMessage extends AbstractMessage
{
    /** @var $disableNotification bool|null */
    protected $disableNotification;

    /** @var $replyToMessageId int|null */
    protected $replyToMessageId;

    /** @var $replyMarkUp null|InlineKeyboardMarkup|ReplyKeyboardMarkup|ReplyKeyboardHide|ForceReply */
    protected $replyMarkUp;

    /**
     * @return bool|null
     */
    public function getDisableNotification(): ?bool
    {
        return $this->disableNotification;
    }

    /**
     * @param bool|null $disableNotification
     * @return SimpleMessage
     */
    public function setDisableNotification(?bool $disableNotification): SimpleMessage
    {
        $this->disableNotification = $disableNotification;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getReplyToMessageId(): ?int
    {
        return $this->replyToMessageId;
    }

    /**
     * @param int|null $replyToMessageId
     * @return SimpleMessage
     */
    public function setReplyToMessageId(?int $replyToMessageId): SimpleMessage
    {
        $this->replyToMessageId = $replyToMessageId;
        return $this;
    }

    /**
     * @return null|ForceReply|InlineKeyboardMarkup|ReplyKeyboardHide|ReplyKeyboardMarkup
     */
    public function getReplyMarkUp()
    {
        return $this->replyMarkUp;
    }

    /**
     * @param null|ForceReply|InlineKeyboardMarkup|ReplyKeyboardHide|ReplyKeyboardMarkup $replyMarkUp
     * @return SimpleMessage
     */
    public function setReplyMarkUp($replyMarkUp)
    {
        $this->replyMarkUp = $replyMarkUp;
        return $this;
    }
}