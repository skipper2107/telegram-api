<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 3:59 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Traits\HasAttachment;

class VoiceMessage extends SimpleMessage
{
    use HasAttachment;

    /** @var $duration int|null */
    protected $duration;

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return VoiceMessage
     */
    public function setDuration(?int $duration): VoiceMessage
    {
        $this->duration = $duration;
        return $this;
    }
}