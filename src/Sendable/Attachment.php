<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/12/18
 * Time: 3:40 PM
 */

namespace Skipper\Telegram\Sendable;

class Attachment
{
    /** @var $fileId string|null */
    protected $fileId;
    /** @var $fileLocation string|null */
    protected $fileLocation;

    /**
     * @return null|string
     */
    public function getFileId(): ?string
    {
        return $this->fileId;
    }

    /**
     * @param null|string $fileId
     * @return Attachment
     */
    public function setFileId(?string $fileId): Attachment
    {
        $this->fileId = $fileId;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFileLocation(): ?string
    {
        return $this->fileLocation;
    }

    /**
     * @param null|string $fileLocation
     * @return Attachment
     */
    public function setFileLocation(?string $fileLocation): Attachment
    {
        $this->fileLocation = $fileLocation;
        return $this;
    }
}