<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 12:24 PM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Enumerators\ChatActionEnumerator;

class ChatActionMessage extends AbstractMessage
{
    /** @var $action string */
    protected $action;

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return ChatActionMessage
     * @throws \ReflectionException
     */
    public function setAction(string $action): ChatActionMessage
    {
        $this->action = (new ChatActionEnumerator)->normalize($action);
        return $this;
    }
}