<?php
/**
 * Created by PhpStorm.
 * User: skipper
 * Date: 3/29/18
 * Time: 11:58 AM
 */

namespace Skipper\Telegram\Sendable;

use Skipper\Telegram\Contracts\VenueContract;
use Skipper\Telegram\Traits\HasVenue;

class VenueMessage extends LocationMessage implements VenueContract
{
    use HasVenue;
}